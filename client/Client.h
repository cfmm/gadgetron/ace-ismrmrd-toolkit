//
//  Client.h
//  ACE Client for gadgetron
//
//  Created by Martyn Klassen on 2016-09-26.
//  Copyright © 2016 CFMM. All rights reserved.
//

#ifndef Client_h
#define Client_h

#include <ismrmrd/ismrmrd.h>
#include "TransportClient.h"

namespace network
{
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedStructInspection"

   class Client
   {
   public:
      Client()
            : m_Client(nullptr)
      {};

      virtual ~Client()
      {
         if (m_Client) this->close();
      };

      int connect(const std::string &host, u_short port);

      int close();

      template<typename T>
      int send(const ISMRMRD::Image<T> &image);

      int send(const ISMRMRD::Acquisition &acq);

      int config(ACE_Message_Block *mb);

      void setFile(const std::string &filename)
      { m_sConfigFile = filename; }

      void setScript(const std::string &script)
      { m_sConfigScript = script; }

      void addParameter(const std::string &parameter)
      { m_vParameters.push_back(parameter); }

      void clearParameters()
      { m_vParameters.clear(); }

   private:
      TransportClient *m_Client;
      std::vector<std::string> m_vParameters;
      std::string m_sConfigFile;
      std::string m_sConfigScript;

      // Copy constructor and copy assignment are not allowed
      Client(const Client &);

      Client &operator=(const Client &);
   };

   template<typename T>
   int Client::send(const ISMRMRD::Image<T> &image)
   {
      if (!m_Client) return -1;

      size_t dataSize = image.getDataSize();
      ACE_Message_Block *data = nullptr;
      ACE_NEW_RETURN(data, ACE_Message_Block(dataSize + 8), -1);

      data->rd_ptr(ACE_ptr_align_binary(data->base(), sizeof(T)));
      data->wr_ptr(data->rd_ptr());

      memcpy(data->wr_ptr(), image.getDataPtr(), dataSize);
      data->wr_ptr(dataSize);

      std::string attributes;
      image.getAttributeString(attributes);

      return m_Client->send(image.getHead(), attributes, data);
   }

#pragma clang diagnostic pop

}

#endif /* Client_h */



