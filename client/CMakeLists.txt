include_directories(
      ${CMAKE_CURRENT_SOURCE_DIR}
      ${CMAKE_CURRENT_SOURCE_DIR}/../protocol
      ${ISMRMRD_INCLUDE_DIR}
      ${ACE_INCLUDE_DIR}
)

add_library(cfmm_client SHARED
      Client.cpp
      TransportClient.cpp
      )

target_link_libraries(cfmm_client
      cfmm_protocol
      optimized ${ACE_LIBRARIES} debug ${ACE_DEBUG_LIBRARY})


set_target_properties(cfmm_client PROPERTIES VERSION ${GADGETRON_VERSION_STRING} SOVERSION ${GADGETRON_SOVERSION})

install(TARGETS cfmm_protocol DESTINATION lib COMPONENT main)

install(FILES
      Client.h
      TransportClient.h
      DESTINATION include COMPONENT main)