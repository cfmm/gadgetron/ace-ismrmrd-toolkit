//
//  Client.cpp
//  network
//
//  Created by Martyn Klassen on 2016-09-26.
//  Copyright © 2016 CFMM. All rights reserved.
//

#include "Client.h"
#include "../protocol/GadgetronMessages.h"


namespace network
{
   int Client::connect(const std::string &host, u_short port)
   {
      if (m_Client)
         return 0;

      TransportClient *client = new TransportClient;
      ACE_Connector<TransportClient, ACE_SOCK_CONNECTOR> connector(ACE_Reactor::instance());
      client->reference_counting_policy().value(ACE_Event_Handler::Reference_Counting_Policy::ENABLED);
      ACE_INET_Addr addr(port, host.c_str());
      if (connector.connect(client, addr) != 0)
      {
         DEBUG_STREAM("Unable to connect to server\n");
         delete client;
         return -1;
      }

      int retval = 0;
      if (client->send(GADGET_MESSAGE_CONFIG_FILE, m_sConfigFile) != 0)
      {
         DEBUG_STREAM("Unable to send configuration file\n");
         retval = -1;
      }

      if (retval || (client->send(GADGET_MESSAGE_CONFIG_SCRIPT, m_sConfigScript) != 0))
      {
         DEBUG_STREAM("Unable to send configuration script\n");
         retval = -1;
      }

      //Send XML parameters
      std::vector<std::string>::const_iterator iterParameter(m_vParameters.begin());
      std::vector<std::string>::const_iterator iterEnd(m_vParameters.end());

      while (iterParameter != iterEnd)
      {
         if (retval || (client->send(GADGET_MESSAGE_PARAMETER_SCRIPT, *iterParameter) != 0))
         {
            DEBUG_STREAM("Unable to send parameters\n");
            retval = -1;
            break;
         }
         iterParameter++;
      }

      if (retval)
      {
         client->flush();
         client->send(GADGET_MESSAGE_CLOSE);
         client->wait();
      }
      else
      {
         m_Client = client;
      }
      return retval;
   }

   int Client::close()
   {
      if (m_Client)
      {
         m_Client->send(GADGET_MESSAGE_CLOSE);
         m_Client->wait();
         m_Client = nullptr;
      }
      return 0;
   }

   int Client::send(const ISMRMRD::Acquisition &acq)
   {
      if (!m_Client) return -1;

      size_t dataSize = acq.getDataSize();
      ACE_Message_Block *data = nullptr;
      ACE_NEW_RETURN(data, ACE_Message_Block(dataSize + 8), -1);

      data->rd_ptr(ACE_ptr_align_binary(data->base(), sizeof(complex_float_t)));
      data->wr_ptr(data->rd_ptr());

      memcpy(data->wr_ptr(), acq.getDataPtr(), dataSize);
      data->wr_ptr(dataSize);

      ACE_Message_Block *traj = nullptr;

      dataSize = acq.getTrajSize();
      if (dataSize > 0)
      {
         ACE_NEW_NORETURN(traj, ACE_Message_Block(dataSize + 8));
         if (!traj)
         {
            data->release();
            return -1;
         }

         traj->rd_ptr(ACE_ptr_align_binary(traj->base(), sizeof(complex_float_t)));
         traj->wr_ptr(traj->rd_ptr());

         memcpy(traj->wr_ptr(), acq.getTrajPtr(), dataSize);
         traj->wr_ptr(dataSize);
      }

      // Send releases data on error
      return m_Client->send(acq.getHead(), data, traj);
   }

   int Client::config(ACE_Message_Block *mb)
   {
      if (!m_Client) return -1;
      return m_Client->send(GADGET_MESSAGE_PARAMETER_SCRIPT, mb);
   }
}

