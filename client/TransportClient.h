//
//  TransportClient.h
//  Network
//
//  Created by Martyn Klassen on 2015-03-11.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#ifndef __Network__TransportClient__
#define __Network__TransportClient__

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/SOCK_Stream.h>
#include <ace/Svc_Handler.h>
#include <ace/Connector.h>
#include <ace/SOCK_Connector.h>

#pragma clang diagnostic pop

#include "message_configuration.h"
#include <vector>
#include <ismrmrd/ismrmrd.h>

#if defined (network_EXPORTS)
#define DLL_API ACE_Proper_Export_Flag
#else
#define DLL_API ACE_Proper_Import_Flag
#endif

namespace network
{
   class DLL_API TransportProcessor
   {
   public:
      virtual int process(ACE_Message_Block *mb)
      {
         mb->release();
         return 0;
      }
   };

   // Multi-threaded because we have different threads that are enqueuing and dequeuing
   // Also getq() does not block if ACE_NULL_SYNCH is used
   class DLL_API ProcessorTask : public ACE_Task<ACE_MT_SYNCH>
   {
      typedef ACE_Task<ACE_MT_SYNCH> super;
   public:
      ProcessorTask()
            : m_pDelegate(nullptr)
      {}

      virtual ~ProcessorTask()
      {}

      virtual int open(TransportProcessor *processor);

      virtual int svc();

      virtual int close(u_long flags);

      virtual int close()
      { return close(0); }

   private:
      virtual int open()
      {
         return open(static_cast<void *>(nullptr));
      }

      virtual int open(void *);

      TransportProcessor *m_pDelegate;
   };

   class DLL_API TransportClient : public ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_MT_SYNCH>
   {
      typedef ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_MT_SYNCH> super;
   public:
      TransportClient(ACE_Thread_Manager *thr_mgr = 0,
                      ACE_Message_Queue<ACE_MT_SYNCH> *mq = 0,
                      ACE_Reactor *reactor = ACE_Reactor::instance())
            : super(thr_mgr, mq, reactor)
            , m_Processor()
            , m_mb(nullptr)
            , m_Timeout(0, 10000)
      {}

      virtual ~TransportClient()
      {}

      virtual int open()
      {
         return open(nullptr);
      }

      virtual int open(void *);

      virtual int close(unsigned long flags);

      virtual int handle_input()
      {
         return handle_input(ACE_INVALID_HANDLE);
      }

      virtual int handle_input(ACE_HANDLE fd);

      virtual int handle_close()
      {
         return handle_close(ACE_INVALID_HANDLE, ACE_Event_Handler::ALL_EVENTS_MASK);
      }

      virtual int handle_close(ACE_HANDLE hd)
      {
         return handle_close(hd, ACE_Event_Handler::ALL_EVENTS_MASK);
      }

      virtual int handle_close(ACE_HANDLE hd, ACE_Reactor_Mask mask);

      virtual int handle_signal(int signum)
      {
         return handle_signal(signum, nullptr, nullptr);
      }

      virtual int handle_signal(int signum, siginfo_t *siginfo)
      {
         return handle_signal(signum, siginfo, nullptr);
      }

      virtual int handle_signal(int signum, siginfo_t *siginfo, ucontext_t *context);

      // Active object that handles received messages through it message queue
      virtual int svc();

      // Sets the high and low watermark for the Output Task queue
      void memory(size_t kilobytes);

      ACE_Time_Value const &timeout() const
      {
         return m_Timeout;
      }

      void timeout(ACE_Time_Value &tv)
      {
         m_Timeout = tv;
      }

      // send functions ensure messages are correctly formatted
      int send(ACE_UINT16 type, ACE_Message_Block *mb = nullptr);

      int send(ACE_UINT16 type, const std::string &data);

      int send(const ISMRMRD::ImageHeader &header, const std::string &attributes, ACE_Message_Block *data);

      int send(const ISMRMRD::AcquisitionHeader &header, ACE_Message_Block *data, ACE_Message_Block *traj = nullptr);

      bool readIniFile(const std::string &inifile, std::string &host, std::string &port);

      bool writeIniFile(const std::string &inifile, const std::string &host, const std::string &port);

      bool connect(const std::string &inifile, std::string &host, std::string &port, TransportProcessor *processor);

      void disconnect(bool success);

      bool configure(const std::string &configFile, const std::string &configScript, const std::vector<std::string> &parameters);

      bool configure(const std::string &configFile, const std::string &configScript, const std::string &parameter);

      bool connected()
      {
         return this->thr_count() > 0;
      }

      virtual int put(ACE_Message_Block *mb)
      {
         return put(mb, nullptr);
      }

      virtual int put(ACE_Message_Block *, ACE_Time_Value *);

   private:
      int read_block(ACE_Message_Block *mb);

      int submit();

      ProcessorTask m_Processor;

      TransportClient(const TransportClient &);

      TransportClient operator=(const TransportClient &);

      ACE_Message_Block *m_mb;
      ACE_Time_Value m_Timeout;
   };


   typedef ACE_Connector<TransportClient, ACE_SOCK_CONNECTOR> TransportConnector;

} /* namespace network */

#endif /* defined(__Network__TransportClient__) */
