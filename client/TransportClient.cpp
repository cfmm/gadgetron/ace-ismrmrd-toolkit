//
//  TransportClient.cpp
//  Network
//
//  Created by Martyn Klassen on 2015-03-11.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/CDR_Stream.h>
#include <ace/Configuration.h>
#include <ace/Configuration_Import_Export.h>

#pragma clang diagnostic pop

// Local
#include "TransportClient.h"
#include "GadgetronMessages.h"
#include "MessageParser.h"
#include "MessageWriter.h"

#include <cassert>

#define CONNECTION_TIMEOUT_PERIOD 3


namespace network
{
   class ReaderTask : public ACE_Task<ACE_MT_SYNCH>
   {
      typedef ACE_Task<ACE_MT_SYNCH> super;
   public:
      ReaderTask(TransportClient &client)
            : m_Client(client)
      {}

      virtual ~ReaderTask()
      {}

      virtual int svc();

      virtual int open()
      {
         return this->open(nullptr);
      }

      virtual int open(void *);

      virtual int close(u_long flags);

   private:
      TransportClient &m_Client;
   };

   int ReaderTask::open(void *arg)
   {
      if (super::open(arg) == -1)
         return -1;

      // Start a thread for the task
      return this->activate(THR_NEW_LWP | THR_JOINABLE, 1); // NOLINT(hicpp-signed-bitwise)
   }

   int ReaderTask::close(u_long flags)
   {
      if (this->thr_count() > 1) return super::close(flags);

      // Last thread is completed

      int retValue = super::close(flags);

      // Delete ourselves
      delete this;

      return retValue;
   }

   int ReaderTask::svc()
   {
      while(true)
      {
         ACE_Message_Block *mb;
         ACE_NEW_RETURN(mb, ACE_Message_Block(sizeof(ACE_UINT16) + sizeof(ACE_UINT32)), -1);

         if (m_Client.peer().recv_n(mb->wr_ptr(), sizeof(ACE_UINT16)) != sizeof(ACE_UINT16))
         {
            ERROR_STREAM("Failed to receive message type ... \n");
            mb->release();
            return -1;
         }

         // Convert message type field from network to host format in-place
         *reinterpret_cast<ACE_UINT16 *>(mb->wr_ptr()) = static_cast<ACE_UINT16>(ACE_NTOHS(*reinterpret_cast<ACE_UINT16 *>(mb->wr_ptr()))); // NOLINT(hicpp-signed-bitwise)
         mb->wr_ptr(sizeof(ACE_UINT16));

         // Check for shutdown message
         if (*reinterpret_cast<ACE_UINT16 *>(mb->rd_ptr()) == GADGET_MESSAGE_CLOSE)
         {
            mb->release();

            // Call handle_close() of the client, just like handle_input() with a reactor would have done
            m_Client.handle_close(m_Client.get_handle(), ACE_Event_Handler::READ_MASK);

            return -1;
         }

         if (m_Client.peer().recv_n(mb->wr_ptr(), sizeof(ACE_UINT32)) != sizeof(ACE_UINT32))
         {
            ERROR_STREAM("Failed to receive message length ... \n");
            mb->release();
            return -1;
         }

         // Convert message type field from network to host format in-place
         *reinterpret_cast<ACE_UINT32 *>(mb->wr_ptr()) = static_cast<ACE_UINT32>(ACE_NTOHS(*reinterpret_cast<ACE_UINT16 *>(mb->wr_ptr()))); // NOLINT(hicpp-signed-bitwise)
         mb->wr_ptr(sizeof(ACE_UINT32));

         ACE_UINT32 msgLength =  *reinterpret_cast<ACE_UINT32 *>(mb->wr_ptr());
         if (msgLength == 0)
         {
            // Message is released on failure in put()
            if (m_Client.put(mb, nullptr) == -1)
               return -1;
            continue;
         }

         // Allocate space for the message payload with a new message block
         // Include ACE_CDR::MAX_ALIGNMENT so data can be aligned for correct ACE_CDR read
         ACE_Message_Block *payload;
         ACE_NEW_NORETURN(payload, ACE_Message_Block(msgLength + ACE_CDR::MAX_ALIGNMENT));
         if (payload == nullptr)
         {
            ERROR_STREAM("Failed to allocate memory for message payload ... \n");
            mb->release();
            return -1;
         }

         // Add the payload to the continuation chain
         mb->cont(payload);

         // Align the memory so that rd_ptr() is on the alignment boundary
         payload->rd_ptr(ACE_ptr_align_binary(payload->base(), ACE_CDR::MAX_ALIGNMENT));
         payload->wr_ptr(payload->rd_ptr());

         if (m_Client.peer().recv_n(payload->wr_ptr(), msgLength) != msgLength)
         {
            ERROR_STREAM("Failed to receive message payload ... \n");
            mb->release();
            return -1;
         }

         // Message is released on failure in put()
         if (m_Client.put(mb, nullptr) == -1)
            return -1;

         // Loop for the next message
      }
   }


   int TransportClient::open(void *p)
   {
      // Default is to register for READ events, i.e receiving from Gadgetron
      if (super::open(p) == -1)
         return -1;

      // Need to use a reader task when there is no reactor
      if (this->reactor() == nullptr)
      {
         ReaderTask *reader = new ReaderTask(*this);
         reader->open();
      }
      else
      {
         // Register for broken pipe signals
         if (this->reactor()->register_handler(SIGPIPE, this) == -1)
            return -1;
      }

      return this->activate(THR_NEW_LWP | THR_JOINABLE, 1); // NOLINT(hicpp-signed-bitwise)
   }

   int TransportClient::close(unsigned long flags)
   {
      // Processing threads are still active
      if (this->thr_count() > 1) return 0;

      // Deactivate the message queue and throw away all remaining messages
      this->flush();

      // We have sent the shutdown message and we need to wait until we have received all the results
      // gadgetron does not have a nice way of checking the integrity of the connect and if
      // gadgetron shuts down abnormally we do not want to wait indefinitely.
      // The work-around is to send more GADGET_MESSAGE_CLOSE messages, unsuccessful write means that
      // the connection is broken
      ACE_Message_Block *mb = MessageWriter::wrap(GADGET_MESSAGE_CLOSE, nullptr);
      ACE_Time_Value tv(3);
      while (m_Processor.thr_count() > 0 && static_cast<size_t>(this->peer().send(mb->rd_ptr(), mb->length(), &tv)) == mb->length())
      {
         // Wait three seconds and then test the connection again
         ACE_OS::sleep(tv);
      }

      if (m_Processor.thr_count() >= 1)
      {
         // At this point the connect is broken / shutdown and m_Processor is still processing
         // Put an insurance shutdown message on the m_Processor queue in case gadgetron did not send its own shutdown message
         mb->set_flags(ACE_Message_Block::MB_STOP);
         // We do not care if we are successful, the task may already have shutdown in a race condition
         if (m_Processor.putq(mb) == -1)
         {
            mb->release();

            // Force a shutdown by deactivating the queue
            m_Processor.flush();
         }

         // Wait for completion because of our insurance shutdown message or it is already shutdown
         m_Processor.wait();
      }
      else
      {
         mb->release();
      }

      // Make sure to unregister for read
      if (this->reactor())
         this->reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK);

      return super::close(flags);
   }

   int TransportClient::read_block(ACE_Message_Block *mb)
   {
      size_t readSize;

      // This should never happen given the class method execution logic
      assert(mb->space() != 0);

      // Read in the requested size
      ssize_t resultSize = this->peer().recv_n(mb->wr_ptr(), mb->space(), &m_Timeout, &readSize);

      // Move the write pointer by the number of read bytes
      mb->wr_ptr(readSize);

      // Check the return status for an error condition
      if (resultSize == -1)
      {
         if (errno == ETIME)
         {
            // Read timed out, so we can try again later
            DEBUG_STREAM("Read " << readSize << " of " << (mb->space() + readSize) << " before timeout.\n");
            return 1;
         }
         else
         {
            // Read failed in a unrecoverable fashion
            if (mb == m_mb)
            {
               ERROR_STREAM("Failed to read message type and length ... \n");
            }
            else
            {
               ERROR_STREAM("Failed to read message payload ... \n");
            }
            // Delete the message block for the caller
            m_mb->release();
            m_mb = nullptr;
            return -1;
         }
      }
      else if (resultSize == 0)
      {
         // EOF reached, which means nothing more can ever be read
         // Submit any existing messages and quit
         submit();
         return -1;
      }
      return 0;
   }

   int TransportClient::put(ACE_Message_Block *mb, ACE_Time_Value *tv)
   {
      assert(mb != nullptr);

      // Enqueue the message
      int result = m_Processor.putq(mb, tv);

      // On failure release the message block
      if (result == -1)
      {
         mb->release();
         ERROR_STREAM ("Failed to enqueue message for processing ... \n");
      }

      return result;
   }

   int TransportClient::submit()
   {
      int result = this->put(m_mb, nullptr);
      m_mb = nullptr;
      return result;
   }

   int TransportClient::handle_input(ACE_HANDLE)
   {
      // If there is not an active message block create one
      if (m_mb == nullptr)
      {
         ACE_NEW_RETURN(m_mb, ACE_Message_Block(sizeof(ACE_UINT16) + sizeof(ACE_UINT32)), -1);

         // Set the size() so space() only returns the number of bytes required for message type only
         m_mb->size(sizeof(ACE_UINT16));
      }

      int result;
      ACE_Message_Block *payload = nullptr;

      // Check for incomplete read of message type
      // Need to check not only for space() but also the size() to ensure the space() is not for the message length
      if (m_mb->space() > 0 && (m_mb->length() < sizeof(ACE_UINT16)))
      {
         result = this->read_block(m_mb);
         if (result != 0)
            return result;

         // Successful read of the message type is now completed

         // Convert message type field from network to host format in-place
         *reinterpret_cast<ACE_UINT16 *>(m_mb->rd_ptr()) = static_cast<ACE_UINT16>(ACE_NTOHS(*reinterpret_cast<ACE_UINT16 *>(m_mb->rd_ptr()))); // NOLINT(hicpp-signed-bitwise)

         // Check for shutdown message
         if (*reinterpret_cast<ACE_UINT16 *>(m_mb->rd_ptr()) == GADGET_MESSAGE_CLOSE)
         {
            m_mb->release();
            m_mb = nullptr;
            // Not an error, but we want to trigger handle_close and shutdown read handler
            return -1;
         }

         // Expand the size() so space() now includes the bytes for the message length
         m_mb->size(m_mb->size() + sizeof(ACE_UINT32));
      }

      // Arriving at this point means the message type has already been read successfully
      // Check for incomplete read of the message length
      // Size check is not required because message length is the last thing read into m_mb
      // The message payload will be on the continuation chain
      if (m_mb->space())
      {
         result = this->read_block(m_mb);
         if (result != 0)
            return result;

         // Successful read of the message length is now completed

         // Convert message length field from network to host format in-place
         // The message length is immediately after the message type
         ACE_UINT32 *msgLength = reinterpret_cast<ACE_UINT32 *>(m_mb->rd_ptr() + sizeof(ACE_UINT16));
         *msgLength = static_cast<ACE_UINT32>(ACE_NTOHL(*msgLength));

         // submit() the empty message if the payload size is zero
         if (*msgLength == 0)
         {
            return this->submit();
         }

         // Allocate space for the message payload with a new message block
         // Include ACE_CDR::MAX_ALIGNMENT so data can be aligned for correct ACE_CDR read
         ACE_NEW_NORETURN(payload, ACE_Message_Block(*msgLength + ACE_CDR::MAX_ALIGNMENT));
         if (payload == nullptr)
         {
            ERROR_STREAM("Failed to allocate memory for message payload ... \n");
            m_mb->release();
            m_mb = nullptr;
            return -1;
         }

         // Add the payload to the continuation chain
         m_mb->cont(payload);

         // Align the memory so that rd_ptr() is on the alignment boundary
         payload->rd_ptr(ACE_ptr_align_binary(payload->base(), ACE_CDR::MAX_ALIGNMENT));
         payload->wr_ptr(payload->rd_ptr());

         // Set the size of the message such that space() returns the required data to read, i.e *msgLength
         // Because payload->rd_ptr() - payload->base() < ACE_CDR::MAX_ALIGNMENT
         // the computed size must necessarily be less than or equal to the ACE_Data_Block::max_size_ and
         // will not trigger a reallocation and potential alignment issue.
         // This will however remove any extra trailing alignment bytes from the capacity() of the ACE_Message_Block
         payload->size(payload->rd_ptr() - payload->base() + *msgLength);
      }

      // At this point the payload message block has been allocated and assigned to the m_mb continuation chain
      // There is also at least some data remaining to be read into the payload and that is given by space()

      // Get the payload from the continuation chain in case this is a retry of reading the payload
      payload = m_mb->cont();

      // Logic demands that payload will never be nullptr
      assert(payload != nullptr);

      result = read_block(payload);
      if (result != 0)
         return result;

      // At this point the entire message has been read and can be enqueued
      return submit();
   }

   int TransportClient::handle_signal(int signum, siginfo_t *, ucontext_t *)
   {
      if (signum == SIGPIPE)
      {
         // Pipe is broken so deregister reade as there will be no new messages
         this->reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK);
      }
      return 0;
   }

   int TransportClient::handle_close(ACE_HANDLE handle, ACE_Reactor_Mask mask)
   {
      if (mask & ACE_Event_Handler::READ_MASK)
      {
         // Handler for read has been removed, probably due to -1 return of handle_input()
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_NORETURN(mb, ACE_Message_Block());
         if (mb == nullptr)
         {
            m_Processor.flush();
            this->flush();
         }
         else
         {
            mb->set_flags(mb->flags() | ACE_Message_Block::MB_STOP);

            if (m_Processor.thr_count() >= 1)
            {
               // Make sure that m_Processor has a shutdown message
               ACE_Message_Block *temp = mb->duplicate();
               if (m_Processor.putq(temp) == -1)
               {
                  // Failed to enqueue the shutdown message
                  temp->release();
                  // Flush deactivates the queue, which triggers failure on the next getq(), and deletes all enqueued messages
                  // This is harsh, but necessary since we could not put STOP message on the queue
                  m_Processor.flush();
               }
            }

            // If we are not receiving messages from gadgetron, then we also cannot send messages so we can shutdown
            // TransportClient::svc()
            if (this->thr_count() >= 1)
            {
               // Send a stop message to the output queue
               if (this->putq(mb) == -1)
               {
                  mb->release();
                  // Failure put the close message is terminal, force quit by flushing which deactivates the queue
                  this->flush();
               }
            }
            else
               mb->release();
         }
      }
      return super::handle_close(handle, mask);
   }

   void TransportClient::memory(size_t kilobytes)
   {
      kilobytes = std::min(kilobytes, static_cast<size_t>(std::numeric_limits<int>::max() / 1024));
      size_t required = kilobytes * 1024;
      m_Processor.msg_queue()->high_water_mark(required);
      m_Processor.msg_queue()->low_water_mark(required);
      this->msg_queue()->high_water_mark(required);
      this->msg_queue()->low_water_mark(required);
   }

   bool TransportClient::connect(const std::string &iniFile, std::string &host, std::string &port, TransportProcessor *processor)
   {
      if (!iniFile.empty() && !this->readIniFile(iniFile, host, port))
         return false;

      TransportConnector connector(this->reactor());
      ACE_INET_Addr addr(port.c_str(), host.c_str());

      TransportClient *me = this;
      if (connector.connect(me, addr) == -1)
      {
         ERROR_STREAM("Unable to open connection to " << host << ":" << port);
         this->flush();
         return false;
      }

      if (m_Processor.open(processor) == -1)
      {
         // Terminate connection with failure
         ERROR_STREAM("Failed to start input processor");
         this->disconnect(false);
         return false;
      }

      m_Processor.msg_queue()->low_water_mark(this->msg_queue()->low_water_mark());
      m_Processor.msg_queue()->high_water_mark(this->msg_queue()->high_water_mark());
      return true;
   }

   void TransportClient::disconnect(bool success)
   {
      if (this->thr_count() >= 1)
      {
         ACE_Time_Value tv(1);

         if (!success)
         {
            // Flush all existing messages because failure has occurred
            this->msg_queue()->flush();

            ACE_Message_Block *data_mb = MessageParser::serialize("ACQUISITION FAILED");
            if (data_mb)
            {
               // Notify gadgetron of failure so it can clean up if required
               if (this->send(GADGET_MESSAGE_PARAMETER_SCRIPT, data_mb) != 0)
               {
                  ERROR_STREAM("Unable to send acquisition failure message");

                  ACE_Message_Block *mb = MessageWriter::wrap(GADGET_MESSAGE_PARAMETER_SCRIPT, data_mb);

                  if (mb)
                  {
                     // Send the failure message directly
                     if (this->peer().send_n(mb, &tv) != static_cast<ssize_t>(mb->total_length()))
                     {
                        ERROR_STREAM("Unable to send acquisition failure message directly");
                     }

                     mb->release();
                  }
                  else
                  {
                     ERROR_STREAM("Failed to wrap acquisition failure message");
                     data_mb->release();
                  }
               }
            }
            else
            {
               ERROR_STREAM("Failed to allocate acquisition failure message");
            }
         }

         // Send the shutdown message
         if (this->send(GADGET_MESSAGE_CLOSE) == -1)
         {
            ERROR_STREAM("Unable to put close message on Queue");

            ACE_Message_Block *mb = MessageWriter::wrap(GADGET_MESSAGE_CLOSE, nullptr);
            if (mb)
            {
               // Send the shutdown message directly
               if (this->peer().send_n(mb, &tv) != static_cast<ssize_t>(mb->total_length()))
               {
                  ERROR_STREAM("Unable to send close message directly");
               }

               mb->release();
            }
            else
            {
               ERROR_STREAM("Unable to allocate direct close message");
            }

            // Force shutdown because sending message failed
            this->flush();
         }
      }

      // Wait for the client to complete all its tasks
      // This waits for svc() to terminate and subsequent close() to complete
      this->wait();
   }

   bool TransportClient::configure(const std::string &configFile, const std::string &configScript, const std::vector<std::string> &parameters)
   {
      if (this->send(GADGET_MESSAGE_CONFIG_FILE, configFile) != 0)
      {
         ERROR_STREAM("Unable to send configuration file");
         return false;
      }

      if (this->send(GADGET_MESSAGE_CONFIG_SCRIPT, configScript) != 0)
      {
         ERROR_STREAM("Unable to send configuration script");
         return false;
      }

      std::vector<std::string>::const_iterator iterParameter(parameters.begin());
      std::vector<std::string>::const_iterator iterEnd(parameters.end());

      while (iterParameter != iterEnd)
      {
         if (this->send(GADGET_MESSAGE_PARAMETER_SCRIPT, *iterParameter) != 0)
         {
            this->flush();
            ERROR_STREAM("Unable to send parameters");
            return false;
         }
         iterParameter++;
      }

      return true;
   }

   bool TransportClient::configure(const std::string &configFile, const std::string &configScript, const std::string &parameters)
   {
      return this->configure(configFile, configScript, std::vector<std::string>(1, parameters));
   }

   bool TransportClient::writeIniFile(const std::string &iniFile, const std::string &host, const std::string &port)
   {
      ACE_Configuration_Heap config;
      if (config.open() == -1)
      {
         ERROR_STREAM("Failed to open configuration");
         return false;
      }

      ACE_TString ace_host(host.c_str());
      ACE_TString ace_port(port.c_str());
      ACE_Configuration_Section_Key key;

      if (config.open_section(config.root_section(), ACE_TEXT("gadgetron"), 1, key) == -1)
      {
         ERROR_STREAM("Unable to find or create gadgetron section in configuration file (writeIniFile)");
         return false;
      }

      if (config.set_string_value(key, ACE_TEXT("hostname"), ace_host) == -1)
      {
         ERROR_STREAM("Unable to SET host name");
         return false;
      }

      if (config.set_string_value(key, ACE_TEXT("port"), ace_port) == -1)
      {
         ERROR_STREAM("Unable to SET host port");
         return false;
      }

      ACE_Ini_ImpExp config_exporter(config);
      if (config_exporter.export_config(iniFile.c_str()) == -1)
      {
         ERROR_STREAM("Unable to export generated configuration to " << iniFile);
         return false;
      }

      return true;
   }

   bool TransportClient::readIniFile(const std::string &iniFile, std::string &host, std::string &port)
   {
      ACE_Configuration_Heap config;
      if (config.open() == -1)
      {
         ERROR_STREAM("Failed to open configuration");
         return false;
      }

      INFO_STREAM("INI file : " << iniFile);

      ACE_Ini_ImpExp config_importer(config);
      if (config_importer.import_config(iniFile.c_str()) == -1)
      {
         if (!(!port.empty() && !host.empty() && writeIniFile(iniFile, host, port)))
         {
            ERROR_STREAM("Unable to create INI file");
            return false;
         }

         if (config_importer.import_config(iniFile.c_str()) == -1)
         {
            ERROR_STREAM("Unable to import configuration file");
            return false;
         }

      }

      ACE_TString ace_host;
      ACE_TString ace_port;
      ACE_Configuration_Section_Key key;
      if (config.open_section(config.root_section(), ACE_TEXT("gadgetron"), 0, key) == -1)
      {
         ERROR_STREAM("Unable to find gadgetron section in configuration file");
         return false;
      }

      if (config.get_string_value(key, ACE_TEXT("hostname"), ace_host) == -1)
      {
         ERROR_STREAM("Unable to determine host name");
         return false;
      }

      if (config.get_string_value(key, ACE_TEXT("port"), ace_port) == -1)
      {
         ERROR_STREAM("Unable to determine port");
         return false;
      }

      host = ace_host.c_str();
      port = ace_port.c_str();

      INFO_STREAM("Found gadgetron server " << host << ":" << port);
      return true;
   }

   int TransportClient::svc()
   {
      ACE_Message_Block *mb = nullptr;

      // Get the next message from the queue (blocking) because ACE_MT_SYNCH is used
      while (-1 != this->getq(mb))
      {

         // Send the message block and its entire chain
         if (this->peer().send_n(mb) != static_cast<ssize_t>(mb->total_length()))
         {
            ERROR_STREAM("Unable to send message");
            mb->release();
            return -1;
         }

         if (mb->flags() & ACE_Message_Block::MB_STOP)
         {
            // Put the message back on the queue for any other threads
            // Unlikely to use multi-threading because it would cause messages to arrive out of order at gadgetron
            // So this is just in case
            this->putq(mb);
            return 0;
         }

         // Release the message as it has now been sent
         mb->release();
      }

      return -1;
   }

   int TransportClient::send(ACE_UINT16 type, ACE_Message_Block *mb)
   {
      ACE_Message_Block *send_mb = MessageWriter::wrap(type, mb);
      if (nullptr == send_mb)
         return -1;

      int retval = 0;

      if (type == GADGET_MESSAGE_CLOSE)
         send_mb->set_flags(send_mb->flags() | ACE_Message_Block::MB_STOP);

      if (this->putq(send_mb) == -1)
      {
         send_mb->release();
         retval = -1;
      }
      return retval;
   }

   int TransportClient::send(ACE_UINT16 type, const std::string &data)
   {
      if (data.empty()) return 0;
      ACE_Message_Block *mb = MessageParser::serialize(data);
      if (nullptr == mb)
         return -1;
      return this->send(type, mb);
   }

   int TransportClient::send(const ISMRMRD::ImageHeader &header, const std::string &attributes, ACE_Message_Block *data)
   {
      IsmrmrdImageMessageParser parser;
      ACE_Message_Block *mb = parser.serialize(header, attributes, data);
      return this->send(GADGET_MESSAGE_ISMRMRD_IMAGE, mb);
   }

   int TransportClient::send(const ISMRMRD::AcquisitionHeader &header, ACE_Message_Block *data, ACE_Message_Block *traj)
   {
      IsmrmrdAcquisitionMessageParser parser;
      ACE_Message_Block *mb = parser.serialize(header, data, traj);
      return this->send(GADGET_MESSAGE_ISMRMRD_ACQUISITION, mb);
   }


   int ProcessorTask::open(TransportProcessor *arg)
   {
      if (nullptr == arg)
         return -1;

      m_pDelegate = arg;
      return open();
   }

   int ProcessorTask::open(void *arg)
   {
      if (super::open(arg) == -1)
         return -1;

      return this->activate(THR_NEW_LWP | THR_JOINABLE, 1); // NOLINT(hicpp-signed-bitwise)
   }

   int ProcessorTask::close(u_long flags)
   {
      if (this->thr_count() > 1) return 0;

      // Deactivate the message queue and throw away any remaining messages
      this->flush();

      return super::close(flags);
   }

   int ProcessorTask::svc()
   {
      ACE_Message_Block *mb = nullptr;
      // Dequeuing is a blocking task
      while (-1 != this->getq(mb))
      {
         if (mb->flags() & ACE_Message_Block::MB_STOP)
         {
            // Put the message back of any other threads
            this->putq(mb);
            return 0;
         }
         if (m_pDelegate->process(mb) == -1)
         {
            return -1;
         }
      }
      return 0;
   }

} /* namespace network */
