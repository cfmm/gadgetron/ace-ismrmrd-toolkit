/*
 * Data conversion routines
 * Data placed into a cross-platform stream, e.g. network,
 * must be converted to a common format and not just sent
 * in the native format of the client.
 *
 * Most clients today are little endian and IEEE 754, so 
 * this is chosen as the default format.
 *
 * Only little-big endian conversions are implemented.
 * Handling mixed, variable endian and alternate float
 * encoding requires additional specialization functions.
 *
 * Authors:
 *  L. Martyn Klassen (mklassen@robarts.ca)
 */

#pragma once
#ifndef _CONVERSION_H
#define _CONVERSION_H

#include <stdexcept>
#include <complex>

// Data structures are streamed in little endian format as this is the dominant
// Intel format and it reduces the required byte swapping.

// Unfortunately C/C++ endianness is extremely messy. Although using hton* functions
// would make sense, many compilers do not support htole* functions that would be
// required, so we roll our own conversions.

// endian.h location is platform dependent
#if defined(__APPLE__)

#include <machine/endian.h>

#elif defined(_MSC_VER)

// Microsoft has no standard way of doing these checks
// but it is also almost universally little-endian
#define BIG_ENDIAN 4321
#define LITTE_ENDIAN 1234
#ifdef NON_INTEL_BYTE_ORDER
#define BYTE_ORDER BIG_ENDIAN
#else
#define BYTE_ORDER LITTLE_ENDIAN
#endif

#ifndef HAS_INT_TYPE
typedef __int16 int16_t;
typedef unsigned __int16 uint16_t;
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
#endif

#else

#include <endian.h>

#endif

// Byteswapping is highly compiler and platform specific
#if defined(_MSC_VER) && _MSC_VER >= 1600

// Microsoft compiler
#include <stdlib.h>

#define local_byteswap16(x) _byteswap_ushort(x)
#define local_byteswap32(x) _byteswap_ulong(x)
#define local_byteswap64(x) _byteswap_uint64(x)

#elif defined(__GNUC__)

// GCC compiler
// There are many versions and really early version might
// not have the builtin functions, in which case this might
// need to be changed

#define local_byteswap16(x) ((((x) & 0xff) << 8u) | ((x) >> 8u)) // NOLINT(hicpp-signed-bitwise)
#define local_byteswap32(x) __builtin_bswap32(x)
#define local_byteswap64(x) __builtin_bswap64(x)

#else

// Other compilers use macros
#define local_byteswap16(value) \
   (uint16_t)((((value) & 0xff) << 8) | ((value) >> 8))
#define local_byteswap32(value) \
   (((uint32_t)local_byteswap16((uint16_t)((value) & 0xffff)) << 16) | \
   (uint32_t)local_byteswap16((uint16_t)((value) >> 16)))
#define local_byteswap64(value) \
   (((uint64_t)local_byteswap32((uint32_t)((value) & 0xffffffff)) << 32) | \
   (uint64_t)local_byteswap32((uint32_t)((value) >> 32)))

#endif

// Check that BYTE_ORDER is actually defined
#ifndef BYTE_ORDER
#error "BYTE_ORDER is not defined"
#endif

namespace network
{

   // Generic conversion declaration
   // No implementation is provided, so it must be specialized for all
   // used functions
   template<typename T>
   inline void byteswap(T *buffer, size_t nValues);

   // Specializations that byte swap from big to little endian
   // Primary byte swapping functions for the 16, 32, and 64 bits
   template<>
   inline void byteswap(uint16_t *buffer, size_t nValues)
   {
      for (size_t i = 0; i < nValues; i++)
         buffer[i] = static_cast<uint16_t> (local_byteswap16(buffer[i]));
   }

   template<>
   inline void byteswap(uint32_t *buffer, size_t nValues)
   {
      for (size_t i = 0; i < nValues; i++)
         buffer[i] = static_cast<uint32_t> (local_byteswap32(buffer[i]));
   }

   template<>
   inline void byteswap(uint64_t *buffer, size_t nValues)
   {
      for (size_t i = 0; i < nValues; i++)
         buffer[i] = static_cast<uint64_t> (local_byteswap64(buffer[i]));
   }

   // Specializations for types that cast to one of the primary functions
   template<>
   inline void byteswap(std::complex<double> *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint64_t *>(buffer), nValues * 2);
   }

   template<>
   inline void byteswap(std::complex<float> *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint32_t *>(buffer), nValues * 2);
   }

   template<>
   inline void byteswap(double *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint64_t *>(buffer), nValues);
   }

   template<>
   inline void byteswap(float *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint32_t *>(buffer), nValues);
   }

   template<>
   inline void byteswap(int64_t *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint64_t *>(buffer), nValues);
   }

   template<>
   inline void byteswap(int32_t *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint32_t *>(buffer), nValues);
   }

   template<>
   inline void byteswap(int16_t *buffer, size_t nValues)
   {
      byteswap(reinterpret_cast<uint16_t *>(buffer), nValues);
   }

// Define conversion based on machine endianness
#if BYTE_ORDER == BIG_ENDIAN

   // Conversion to and from stream
   // Special to and from functions are used because the conversion my
   // not be as simple as byte swapping
   // Currently it is assumed that IEEE 754 is always used
   template <typename T> inline void convertFromStream(T *buffer, size_t nValues)
   {
       byteswap(buffer, nValues);
   }

   template <typename T> inline void convertToStream(T *buffer, size_t nValues)
   {
       byteswap(buffer, nValues);
   }

#elif BYTE_ORDER == LITTLE_ENDIAN

   // Conversion to and from stream
   // Little Endian systems are no-op
   // Currently it is assumed that IEEE 754 is always used
   template<typename T>
   inline void convertFromStream(T * /*buffer*/, size_t /*nValues*/)
   {}

   template<typename T>
   inline void convertToStream(T * /*buffer*/, size_t /*nValues*/)
   {}

#else

#error "Undetermined system endianness"

#endif

} // namespace network

#endif /* _CONVERSION_H */
