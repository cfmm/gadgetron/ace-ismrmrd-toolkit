//
// Created by Martyn Klassen on 2019-02-11.
//

#ifndef ACE_ISMRMRD_TOOLKIT_ISMRMRDACQUISITIONMESSAGEREADER_H
#define ACE_ISMRMRD_TOOLKIT_ISMRMRDACQUISITIONMESSAGEREADER_H

#include "MessageReader.h"
#include "MessageParser.h"

#include <ismrmrd/ismrmrd.h>


namespace network
{
   class IsmrmrdAcquisitionMessageReader : virtual public MessageReader
   {
   public:
      explicit IsmrmrdAcquisitionMessageReader(StreamInterface const &stream)
            : MessageReader(stream)
      {}

      explicit IsmrmrdAcquisitionMessageReader(ACE_SOCK_Stream *stream)
            : MessageReader(stream)
      {}

      virtual ~IsmrmrdAcquisitionMessageReader()
      {}

      virtual MessageParser &parser()
      {
         return m_Parser;
      }

      virtual int read(ISMRMRD::AcquisitionHeader &header)
      {
         ACE_Message_Block *mb = read_header();

         if (nullptr == mb)
            return -1;

         int retValue = 0;

         if (!m_Parser.deserialize(mb, header))
            retValue = -1;

         mb->release();
         return retValue;
      }

      virtual int read(ISMRMRD::AcquisitionHeader &header, ISMRMRD::NDArray<complex_float_t> &data, ISMRMRD::NDArray<float> &traj)
      {
         if (-1 == read(header))
            return -1;

         std::vector<size_t> dims(2);
         dims[0] = header.number_of_samples;
         dims[1] = header.active_channels;

         data.resize(dims);

         if (-1 == read(data.getDataPtr(), data.getNumberOfElements()))
            return -1;

         if (header.trajectory_dimensions)
         {
            //build trajectory
            dims[1] = header.trajectory_dimensions;
            traj.resize(dims);

            if (-1 == read(traj.getDataPtr(), traj.getNumberOfElements()))
               return -1;
         }

         return 0;
      }

      virtual int read(ISMRMRD::Acquisition &acquisition)
      {
         ISMRMRD::AcquisitionHeader &header = const_cast<ISMRMRD::AcquisitionHeader &>(acquisition.getHead());

         if (-1 == read(header))
            return -1;

         acquisition.setHead(header);

         if (-1 == read(acquisition.getDataPtr(), acquisition.getNumberOfDataElements()))
            return -1;

         if (-1 == read(acquisition.getTrajPtr(), acquisition.getNumberOfTrajElements()))
            return -1;

         return 0;
      }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "HidingNonVirtualFunction"

      template<typename T>
      int read(T *data, size_t elements)
      {
         return MessageReader::read(data, elements);
      }

#pragma clang diagnostic pop

   private:
      IsmrmrdAcquisitionMessageParser m_Parser;
   };
}


#endif //ACE_ISMRMRD_TOOLKIT_ISMRMRDACQUISITIONMESSAGEREADER_H
