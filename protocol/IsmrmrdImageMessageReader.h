//
// Created by Martyn Klassen on 2019-02-11.
//

#ifndef ACE_ISMRMRD_TOOLKIT_ISMRMRDIMAGEMESSAGEREADER_H
#define ACE_ISMRMRD_TOOLKIT_ISMRMRDIMAGEMESSAGEREADER_H

#include "MessageReader.h"
#include "MessageParser.h"

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/String_Base.h>

#pragma clang diagnostic pop

#include <ismrmrd/meta.h>
#include <ismrmrd/ismrmrd.h>


namespace network
{

   class IsmrmrdImageMessageReader : virtual public MessageReader
   {
   public:
      explicit IsmrmrdImageMessageReader(StreamInterface const &stream)
            : MessageReader(stream)
      {}

      explicit IsmrmrdImageMessageReader(ACE_SOCK_Stream *stream)
            : MessageReader(stream)
      {}

      virtual ~IsmrmrdImageMessageReader()
      {}

      virtual MessageParser &parser()
      {
         return m_Parser;
      }

      virtual int read(ISMRMRD::ImageHeader &header, ISMRMRD::MetaContainer &meta)
      {
         ACE_Message_Block *mb = read_header();

         if (nullptr == mb)
            return -1;

         int retValue = 0;
         std::string attributes;
         if (!m_Parser.deserialize(mb, header, attributes))
            retValue = -1;
         else if (attributes.length() > 0)
            ISMRMRD::deserialize(attributes.c_str(), meta);

         mb->release();

         return retValue;
      }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "HidingNonVirtualFunction"

      template<typename T>
      int read(T *data, size_t elements)
      {
         return MessageReader::read(data, elements);
      }

#pragma clang diagnostic pop
   private:
      IsmrmrdImageMessageParser m_Parser;
   };

}

#endif //ACE_ISMRMRD_TOOLKIT_ISMRMRDIMAGEMESSAGEREADER_H
