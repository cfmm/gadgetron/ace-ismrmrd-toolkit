#ifndef _MESSAGES_CONFIGURATION_H
#define _MESSAGES_CONFIGURATION_H

#ifdef SIEMENS_IDEA

#include "MrCommon/MrGlobalDefinitions/MrBasicTypes.h"
#define HAS_INT_TYPE 1

#include "MrServers/MrVista/include/Parc/Trace/IceTrace.h"

#define DEBUG_STREAM(message)      ICE_OUT(message)
#define INFO_STREAM(message)       ICE_OUT(message)
#define WARN_STREAM(message)       ICE_WARN(message)
#define ERROR_STREAM(message)      ICE_ERR(message)
#define VERBOSE_STREAM(message)    ICE_TRACE_R(TRACE_10, message)

#else

#if !defined(USE_GADGETRON) || USE_GADGETRON

#include "log.h"

#else
#define GINFO_STREAM(message)    std::cout << message << std::endl; // NOLINT(misc-macro-parentheses)
#define GERROR_STREAM(message)   std::cerr << message << std::endl; // NOLINT(misc-macro-parentheses)
#define GWARN_STREAM(message)    std::cerr << message << std::endl; // NOLINT(misc-macro-parentheses)

#ifdef NDEBUG
#define GDEBUG_STREAM(message)
#else
#define GDEBUG_STREAM(message)   std::cout << message << std::endl; // NOLINT(misc-macro-parentheses)
#endif

#endif

#define ERROR_STREAM(message) GERROR_STREAM(message)
#define INFO_STREAM(message)  GINFO_STREAM(message)
#define WARN_STREAM(message)  GWARN_STREAM(message)
#define DEBUG_STREAM(message) GDEBUG_STREAM(message)

#endif

#endif
