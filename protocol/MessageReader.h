//
// Created by Martyn Klassen on 2019-02-11.
//

#ifndef ACE_ISMRMRD_TOOLKIT_MESSAGEREADER_H
#define ACE_ISMRMRD_TOOLKIT_MESSAGEREADER_H

#include "message_configuration.h"
#include "StreamInterface.h"
#include "MessageParser.h"

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/SOCK_Stream.h>
#include <ace/Message_Block.h>
#include <ace/CDR_Stream.h>

#pragma clang diagnostic pop


namespace network
{
   class MessageReader
   {
   public:
      explicit MessageReader(StreamInterface const &stream)
            : m_pStream(&stream)
            , m_pAllocatedStream(nullptr)
            , m_iDataSize(0)
      {}

      explicit MessageReader(ACE_SOCK_Stream *stream)
            : m_iDataSize(0)
      {
         m_pAllocatedStream = new StreamInterface(stream);
         m_pStream = m_pAllocatedStream;
      }

      virtual ~MessageReader()
      {}

      virtual ACE_Message_Block *read()
      {
         ACE_Message_Block *mb = read_header();

         // Read the rest of the message into a second message block
         ACE_Message_Block *data;
         ACE_NEW_RETURN(data, ACE_Message_Block(static_cast<size_t>(m_iDataSize)), nullptr);

         if (m_pStream->recv_n(data->wr_ptr(), static_cast<size_t>(m_iDataSize)) != m_iDataSize)
         {
            mb->release();
            data->release();
            return nullptr;
         }
         data->wr_ptr(static_cast<size_t>(m_iDataSize));

         // Chain together the message blocks and return them
         mb->cont(data);
         return mb;
      }

   protected:
      virtual MessageParser &parser() = 0;

      virtual ACE_Message_Block *read_block()
      {
         // Read the header length
         ACE_UINT32 hdrLength;
         if (m_pStream->recv_n(&hdrLength, sizeof(hdrLength)) != sizeof(hdrLength))
         {
            return nullptr;
         }
         ACE_UINT32 hdrLengthNetwork = hdrLength;
         hdrLength = ACE_NTOHL(hdrLength); // NOLINT(hicpp-signed-bitwise)

         // Read just the header into a new message block
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_RETURN(mb, ACE_Message_Block(hdrLength + sizeof(hdrLength) + ACE_CDR::MAX_ALIGNMENT), nullptr);
         mb->rd_ptr(ACE_ptr_align_binary(mb->base(), ACE_CDR::MAX_ALIGNMENT));
         mb->wr_ptr(mb->rd_ptr());

         // Put the length of the header into the message in network endianness
         mb->copy(reinterpret_cast<char *>(&hdrLengthNetwork), sizeof(hdrLength));

         if (m_pStream->recv_n(mb->wr_ptr(), hdrLength) != static_cast<ssize_t>(hdrLength))
         {
            mb->release();
            return nullptr;
         }
         mb->wr_ptr(hdrLength);
         return mb;
      }

      virtual ACE_Message_Block *read_header()
      {
         // Read the message size
         ACE_UINT32 msgLength;
         if (m_pStream->recv_n(&msgLength, sizeof(msgLength)) != sizeof(msgLength))
         {
            return nullptr;
         }
         msgLength = ACE_NTOHL(msgLength); // NOLINT(hicpp-signed-bitwise)

         // Read the block containing the header
         ACE_Message_Block *mb = read_block();
         if (nullptr == mb)
            return nullptr;

         // Read the block containing the data formatting instructions
         ACE_Message_Block *data_format = read_block();

         if (nullptr == data_format)
         {
            mb->release();
            return nullptr;
         }

         // Store the data formatting information
         network::MessageParser::get_last(mb)->cont(data_format);

         // Record the remaining message size to read
         m_iDataSize = msgLength - mb->total_length();
         return mb;
      }

      template<typename T>
      int read(T *data, size_t elements)
      {
         ssize_t dataSize = elements * sizeof(T);

         if (dataSize > m_iDataSize
             || m_pStream->recv_n(data, static_cast<size_t>(dataSize)) != dataSize
             || !parser().format(data, elements))
            return -1;

         m_iDataSize -= dataSize;
         return 0;
      }

      ssize_t datasize() const
      {
         return m_iDataSize;
      }

   private:
      StreamInterface const *m_pStream;
      StreamInterface *m_pAllocatedStream;
      ssize_t m_iDataSize;
   };
}

#endif //ACE_ISMRMRD_TOOLKIT_MESSAGEREADER_H
