//
// Created by Martyn Klassen on 2019-02-15.
//

#ifndef ACE_ISMRMRD_TOOLKIT_STREAMINTERFACE_H
#define ACE_ISMRMRD_TOOLKIT_STREAMINTERFACE_H

#include <ace/SOCK_Stream.h>

class StreamInterface
{
public:
   virtual ~StreamInterface()
   {}

   explicit StreamInterface(ACE_SOCK_Stream *stream)
         : m_pStream(stream)
   {}

   virtual ssize_t send_n(ACE_Message_Block const *mb) const
   {
      return m_pStream->send_n(mb);
   }

   virtual ssize_t recv_n(void *buf, size_t len) const
   {
      return m_pStream->recv_n(buf, len);
   }

private:
   ACE_SOCK_Stream *m_pStream;
};

#endif //ACE_ISMRMRD_TOOLKIT_STREAMINTERFACE_H
