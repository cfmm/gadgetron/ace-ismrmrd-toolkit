//
// Created by Martyn Klassen on 2019-02-11.
//

#ifndef ACE_ISMRMRD_TOOLKIT_MESSAGEWRITER_H
#define ACE_ISMRMRD_TOOLKIT_MESSAGEWRITER_H

#include "message_configuration.h"
#include "StreamInterface.h"

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/SOCK_Stream.h>
#include <ace/Message_Block.h>

#pragma clang diagnostic pop


namespace network
{
   class MessageWriter
   {
   public:
      explicit MessageWriter(StreamInterface const &stream)
            : m_pStream(&stream)
            , m_pAllocatedStream(nullptr)
            , m_bSwap(false)
      {}

      explicit MessageWriter(ACE_SOCK_Stream *stream)
            : m_bSwap(false)
      {
         m_pAllocatedStream = new StreamInterface(stream);
         m_pStream = m_pAllocatedStream;
      }

      virtual ~MessageWriter()
      {
         delete m_pAllocatedStream;
      }

      static ACE_Message_Block *wrap(ACE_UINT16 msgType, ACE_Message_Block *data)
      {
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_RETURN(mb, ACE_Message_Block(sizeof(ACE_UINT16) + sizeof(ACE_UINT32)), nullptr);

         // Record the message type
         msgType = static_cast<ACE_UINT16>(ACE_HTONS(msgType)); // NOLINT(hicpp-signed-bitwise)
         mb->copy(reinterpret_cast<const char *>(&msgType), sizeof(msgType));

         if (nullptr != data)
         {
            // Record the message length
            ACE_UINT32 msgLength = static_cast<ACE_UINT32>(data->total_length());
            msgLength = ACE_HTONL(msgLength); // NOLINT(hicpp-signed-bitwise)
            mb->copy(reinterpret_cast<const char *>(&msgLength), sizeof(msgLength));

            mb->cont(data);
         }
         return mb;
      }

   protected:

      virtual int write(ACE_Message_Block *output)
      {
         ACE_Message_Block *mb = wrap(message_id(), output);
         if (nullptr == mb)
            return -1;
         int retVal = 0;
         if (m_pStream->send_n(mb) != static_cast<ssize_t>(mb->total_length()))
            retVal = -1;
         mb->release();
         return retVal;
      }

      virtual ACE_UINT16 message_id() const = 0;

   private:
      StreamInterface const *m_pStream;
      StreamInterface *m_pAllocatedStream;
      bool m_bSwap;
   };
}


#endif //ACE_ISMRMRD_TOOLKIT_MESSAGEWRITER_H
