//
//  DicomMetaContainer.h
//  network
//
//  Created by Sahar Rabinoviz on 2015-08-14.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#ifndef __network__DicomMetaContainer__
#define __network__DicomMetaContainer__

#include <cstdio>

#include "meta_key_def.h"
#include "dcmtk/ofstd/oftypes.h"

#define DICOM_KEY_IMAGE_SCALE_OFFSET        DicomKeySet(GADGETRON_IMAGE_SCALE_OFFSET,   0x0028, 0x1052,   T_DOUBLE)
#define DICOM_KEY_IMAGE_SCALE_RATIO         DicomKeySet(GADGETRON_IMAGE_SCALE_RATIO,    0x0028, 0x1053,   T_DOUBLE)
#define DICOM_NUMBER_PHASE_ENCODE_STEPS     DicomKeySet(NUMBER_PHASE_ENCODE_STEPS,      0x0018, 0x0089,   T_LONG)
#define DICOM_NUMBER_AVERAGES               DicomKeySet(NUMBER_AVERAGES,                0x0018, 0x0083,   T_LONG)
#define DICOM_OPERATOR_NAME                 DicomKeySet(OPERATOR_NAME,                  0x0008, 0x1070,   T_STRING)
#define DICOM_IMAGED_NUCLEUS                DicomKeySet(IMAGED_NUCLEUS,                 0x0018, 0x0085,   T_STRING)
#define DICOM_RF_COIL_NAME                  DicomKeySet(RF_COIL_NAME,                   0x0018, 0x1250,   T_STRING)
#define DICOM_PERCENT_SAMPLING              DicomKeySet(PERCENT_SAMPLING,               0x0018, 0x0093,   T_DOUBLE)
#define DICOM_STUDY_DESCRIPTION             DicomKeySet(STUDY_DESCRIPTION,              0x0008, 0x1030,   T_STRING)
#define DICOM_SERIES_UID                    DicomKeySet(SERIES_UID,                     0x0020, 0x000e,   T_STRING)
#define DICOM_IMAGE_TYPE                    DicomKeySet(IMAGE_TYPE,                     0x0008, 0x0008,   T_STRING)

namespace Gadgetron
{

   typedef enum
   {
      T_STRING = 0,
      T_DOUBLE = 1,
      T_LONG = 2
   } MetaType;

   struct DicomKeySet
   {
      DicomKeySet(std::string n, Uint16 key1, Uint16 key2, MetaType type)
            : name(std::move(n)), k1(key1), k2(key2), expected(type)
      {}

      std::string name;
      Uint16 k1, k2;
      MetaType expected;
   };
}

#endif /* defined(__network__DicomMetaContainer__) */
