//
//  meta_key_def.h
//  network
//
//  Created by Sahar Rabinoviz on 2015-08-20.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#ifndef network_meta_key_def_h
#define network_meta_key_def_h

#include "mri_core_def.h"

#define IMAGE_KSPACE_2              "img_kspace_encoding_2"
#define IMAGE_SLAB                  "img_slab"
#define IMAGE_CONTRAST              "img_contrast"
#define IMAGE_PHASE                 "img_phase"
#define IMAGE_REPETITION            "img_repetition"
#define IMAGE_SET                   "img_set"
#define IMAGE_AVERAGE               "img_average"

#define NUMBER_PHASE_ENCODE_STEPS   "num_phase_encode_steps"
#define NUMBER_AVERAGES             "num_averages"
#define OPERATOR_NAME               "operator_name"
#define IMAGED_NUCLEUS              "imaged_nucleus"
#define RF_COIL_NAME                "rf_coil_name"
#define PERCENT_SAMPLING            "percent_sampling"
#define SERIES_UID                  "series_uid"
#define STUDY_DESCRIPTION           "study_description"
#define IMAGE_TYPE                  "image_type"

#endif
