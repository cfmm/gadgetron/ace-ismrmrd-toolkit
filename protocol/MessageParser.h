//
// Created by Martyn Klassen on 2019-02-13.
//

#ifndef GADGETRON_MESSAGEPARSER_H
#define GADGETRON_MESSAGEPARSER_H

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/Message_Block.h>
#include <ace/CDR_Stream.h>

#pragma clang diagnostic pop

#include "message_configuration.h"
#include "conversion.h"
#include <ismrmrd/ismrmrd.h>
#include <ismrmrd/version.h>

#define ISMRMRD_VERSION_WARNING std::string("ISMRMRD Major version mismatch.")


namespace network
{
   class MessageParser
   {
   public:
      MessageParser();

      virtual ~MessageParser()
      {}

      bool deserialize(ACE_Message_Block *mb);

      static ACE_Message_Block *serialize(const std::string &message);

      static bool copy(char *dest, ACE_Message_Block *mb, size_t dataToCopy);

      static ACE_Message_Block *get_last(ACE_Message_Block const *mb);

      static void move(ACE_Message_Block *mb, size_t length);

      static ACE_Message_Block *wrap_length(ACE_Message_Block *mb);

      virtual bool swap() const
      { return m_bSwap; }

      static ACE_Message_Block *shift(ACE_Message_Block *mb);

      static ssize_t getLength(ACE_Message_Block *mb);

      virtual bool configure(ACE_Message_Block *)
      {
         return true;
      }

      virtual ACE_Message_Block *configure()
      {
         return nullptr;
      }

      template<typename T>
      bool format(T *data, size_t elements)
      {
         if (m_bSwap)
            network::byteswap(data, elements);
         return true;
      }

   protected:
      ACE_Message_Block *serialize(ACE_Message_Block *mb);

      virtual bool deserialize(ACE_InputCDR &msg) = 0;

      virtual bool serialize(ACE_OutputCDR &msg) = 0;

      virtual bool setup(ACE_Message_Block *mb);

   private:
      bool m_bSwap;

   };


   class IsmrmrdImageMessageParser : virtual public MessageParser
   {
   public:
      virtual ~IsmrmrdImageMessageParser() // NOLINT(modernize-use-override)
      {}

      virtual bool deserialize(ACE_Message_Block *mb, ISMRMRD::ImageHeader &header, std::string &attributes, char **data);

      virtual bool deserialize(ACE_Message_Block *mb, ISMRMRD::ImageHeader &header, std::string &attributes);

      virtual ACE_Message_Block *serialize(ISMRMRD::ImageHeader const &header, std::string const &attributes)
      {
         return serialize(header, attributes, nullptr);
      }

      virtual ACE_Message_Block *serialize(ISMRMRD::ImageHeader const &header, std::string const &attributes, ACE_Message_Block *data);

   protected:

      virtual bool deserialize(ACE_InputCDR &msg);

      virtual bool serialize(ACE_OutputCDR &msg);

      virtual bool parse_data(ACE_Message_Block *mb, ISMRMRD::ImageHeader &header, char **data);

   private:
      ISMRMRD::ImageHeader *m_pHeader;
      std::string *m_pAttributes;
   };


   class IsmrmrdAcquisitionMessageParser : virtual public MessageParser
   {
   public:
      virtual ~IsmrmrdAcquisitionMessageParser()
      {}

      virtual ACE_Message_Block *serialize(const ISMRMRD::AcquisitionHeader &header)
      {
         return serialize(header, nullptr, nullptr);
      }

      virtual ACE_Message_Block *serialize(const ISMRMRD::AcquisitionHeader &header, ACE_Message_Block *data)
      {
         return serialize(header, data, nullptr);
      }

      virtual ACE_Message_Block *serialize(const ISMRMRD::AcquisitionHeader &header, ACE_Message_Block *data, ACE_Message_Block *traj);

      virtual ACE_Message_Block *serialize(ISMRMRD::Acquisition const &acquisition);

      virtual bool deserialize(ACE_Message_Block *mb, ISMRMRD::AcquisitionHeader &header);

      virtual bool deserialize(ACE_Message_Block *mb, ISMRMRD::Acquisition &acquisition);

   protected:

      virtual bool deserialize(ACE_InputCDR &msg);

      virtual bool serialize(ACE_OutputCDR &msg);

   private:
      ACE_CDR::Boolean write_ismrmrd_encoding_counters(ACE_OutputCDR &msg, const ISMRMRD::ISMRMRD_EncodingCounters &counter);

      ACE_CDR::Boolean read_ismrmrd_encoding_counters(ACE_InputCDR &msg, ISMRMRD::ISMRMRD_EncodingCounters &counter);

   private:
      ISMRMRD::AcquisitionHeader *m_pHeader;
   };
}

#endif //GADGETRON_MESSAGEPARSER_H
