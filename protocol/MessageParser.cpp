//
// Created by Martyn Klassen on 2019-02-13.
//

#include "MessageParser.h"
#include "conversion.h"

// ACE
#pragma warning(disable: 4068)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <ace/String_Base.h>

#pragma clang diagnostic pop


namespace network
{
   MessageParser::MessageParser()
         : m_bSwap(false)
   {}

   ACE_Message_Block *MessageParser::shift(ACE_Message_Block *mb)
   {
      while (nullptr != mb && mb->length() == 0)
         mb = mb->cont();
      return mb;
   }

   ssize_t MessageParser::getLength(ACE_Message_Block *mb)
   {
      mb = shift(mb);
      ACE_UINT32 hdrLength = ACE_NTOHL(*(reinterpret_cast<ACE_UINT32 *>(mb->rd_ptr()))); // NOLINT(hicpp-signed-bitwise)
      mb->rd_ptr(sizeof(hdrLength));
      if (hdrLength > mb->total_length())
         return -1;
      return hdrLength;
   }


   bool MessageParser::setup(ACE_Message_Block *mb)
   {
      ssize_t hdrLength = getLength(mb);
      if (hdrLength < 0)
         return false;
      bool retVal = configure(mb);
      move(mb, static_cast<size_t>(hdrLength));
      return retVal;
   }

   ACE_Message_Block *MessageParser::wrap_length(ACE_Message_Block *data)
   {
      // Prepend the length of the header
      ACE_Message_Block *mb = nullptr;
      ACE_NEW_RETURN(mb, ACE_Message_Block(sizeof(ACE_UINT32)), nullptr);

      ACE_UINT32 hdrLength(0);
      if (nullptr != data)
         hdrLength = static_cast<ACE_UINT32>(data->total_length());
      hdrLength = ACE_HTONL(hdrLength); // NOLINT(hicpp-signed-bitwise)
      mb->copy(reinterpret_cast<char const *>(&hdrLength), sizeof(hdrLength));
      mb->cont(data);
      return mb;
   }

   bool MessageParser::deserialize(ACE_Message_Block *mb)
   {
      // Get the first message block with actual data
      mb = shift(mb);

      ssize_t hdrLength = getLength(mb);

      if (hdrLength < 0)
         return false;

      // Realign the data so the ACE_InputCDR can read properly
      // The memory needs to be aligned with ACE_CDR::MAX_ALIGNMENT
      // Use crunch when base is already aligned
      // Otherwise move the data to be aligned, if possible
      char *aligned = ACE_ptr_align_binary(mb->base(), ACE_CDR::MAX_ALIGNMENT);
      if (aligned != mb->base())
      {
         if ((mb->rd_ptr() > aligned) && (mb->rd_ptr() < mb->wr_ptr()))
         {
            size_t const len = mb->length();
            ACE_OS::memmove(aligned, mb->rd_ptr(), len);
            mb->rd_ptr(aligned);
            mb->wr_ptr(aligned + len);
         }
         else
            return false;
      }
      else if (mb->crunch() == -1)
         return false;

      ACE_InputCDR msg(mb);

      // Set the correct byte order
      ACE_CDR::Boolean byte_order;
      msg >> ACE_InputCDR::to_boolean(byte_order);
      msg.reset_byte_order(byte_order);

      m_bSwap = msg.do_byte_swap();

      bool retValue = deserialize(msg);

      move(mb, static_cast<size_t>(hdrLength));

      return retValue && setup(mb);
   }

   ACE_Message_Block *MessageParser::serialize(ACE_Message_Block *data)
   {
      // Create the header
      // Message blocks are automatically aligned with ACE_CDR::MAX_ALIGNMENT
      ACE_OutputCDR msg;
      // Record the byte order for later extraction
      msg << ACE_OutputCDR::from_boolean(ACE_CDR_BYTE_ORDER);

      m_bSwap = msg.do_byte_swap();

      if (!serialize(msg))
         return nullptr;

      // Get the ACE_CDR serialize header and wrap it in a message containing the length
      ACE_Message_Block *mb = wrap_length(msg.begin()->duplicate());
      if (nullptr == mb)
         return nullptr;

      // Get the data format message and wrap it in a message containing the length
      ACE_Message_Block *data_format = wrap_length(configure());
      if (nullptr == data_format)
      {
         mb->release();
         return nullptr;
      }

      // Append the data after the data format message
      get_last(data_format)->cont(data);

      // Append the data format after the header
      get_last(mb)->cont(data_format);
      return mb;
   }

   ACE_Message_Block *MessageParser::serialize(const std::string &message)
   {
      if (message.empty()) return nullptr;
      ACE_Message_Block *mb = nullptr;
      ACE_NEW_RETURN(mb, ACE_Message_Block(message.length()), nullptr);
      mb->copy(message.c_str(), message.length());
      return mb;
   }

   void MessageParser::move(ACE_Message_Block *mb, size_t length)
   {
      size_t block;
      while (mb && length > 0)
      {
         if (mb->rd_ptr() < mb->wr_ptr())
         {
            block = std::min(length, mb->length());
            mb->rd_ptr(block);
            length -= block;
         }
         mb = mb->cont();
      }
   }

   bool MessageParser::copy(char *dest, ACE_Message_Block *mb, size_t dataToCopy)
   {
      if (dataToCopy == 0 || nullptr == dest || nullptr == mb)
         return true;

      if (mb->total_length() < dataToCopy)
         return false;

      size_t dataBlock;

      // Copy the data into the destination
      while (mb && dataToCopy != 0)
      {
         if (mb->rd_ptr() < mb->wr_ptr())
         {
            dataBlock = std::min(dataToCopy, mb->length());
            memcpy(dest, mb->rd_ptr(), dataBlock);
            mb->rd_ptr(dataBlock);
            dest += dataBlock;
            dataToCopy -= dataBlock;
         }
         mb = mb->cont();
      }

      return 0 == dataToCopy;
   }

   ACE_Message_Block *MessageParser::get_last(ACE_Message_Block const *mb)
   {

      ACE_Message_Block const *next = mb;
      if (nullptr != next)
      {
         while (next->cont() != nullptr)
         {
            next = next->cont();
         }
      }
      return const_cast<ACE_Message_Block *>(next);
   }

   ACE_Message_Block *IsmrmrdImageMessageParser::serialize(ISMRMRD::ImageHeader const &header, std::string const &attributes, ACE_Message_Block *data)
   {
      m_pHeader = const_cast<ISMRMRD::ImageHeader *>(&header);
      m_pAttributes = const_cast<std::string *>(&attributes);
      return MessageParser::serialize(data);
   }

   bool IsmrmrdImageMessageParser::deserialize(ACE_Message_Block *mb, ISMRMRD::ImageHeader &header, std::string &attributes)
   {
      m_pHeader = &header;
      m_pAttributes = &attributes;
      return MessageParser::deserialize(mb);
   }

   bool IsmrmrdImageMessageParser::deserialize(ACE_Message_Block *mb, ISMRMRD::ImageHeader &header, std::string &attributes, char **data)
   {
      if (!deserialize(mb, header, attributes))
         return false;

      return parse_data(mb, header, data);
   }

   bool IsmrmrdImageMessageParser::parse_data(ACE_Message_Block *mb, ISMRMRD::ImageHeader &header, char **data)
   {
      size_t numElements = header.channels * header.matrix_size[0] * header.matrix_size[1] * header.matrix_size[2];
      size_t dataSize = numElements * ISMRMRD::ismrmrd_sizeof_data_type(header.data_type);

      // Check for sufficient data exists
      if (mb->total_length() < dataSize)
         return false;

      // Allocate data
      *data = new char[dataSize];

      if (nullptr == *data)
         return false;

      copy(*data, mb, dataSize);

      if (swap())
      {
         switch (header.data_type)
         {
            case ISMRMRD::ISMRMRD_USHORT:
               byteswap(reinterpret_cast<uint16_t *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_SHORT:
               byteswap(reinterpret_cast<int16_t *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_UINT:
               byteswap(reinterpret_cast<uint32_t *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_INT:
               byteswap(reinterpret_cast<int32_t *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_FLOAT:
               byteswap(reinterpret_cast<float *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_DOUBLE:
               byteswap(reinterpret_cast<double *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_CXFLOAT:
               byteswap(reinterpret_cast<complex_float_t *>(*data), numElements);
               break;
            case ISMRMRD::ISMRMRD_CXDOUBLE:
               byteswap(reinterpret_cast<complex_double_t *>(*data), numElements);
               break;
            default:
               return false;
         }
      }
      return true;
   }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "RedundantCast"

   bool IsmrmrdImageMessageParser::deserialize(ACE_InputCDR &msg)
   {
      ACE_CString attributes;

      bool retVal = (msg.read_ushort(m_pHeader->version) &&
                     msg.read_ushort(m_pHeader->data_type) &&
                     // Some int type madness requires explicit casting
                     msg.read_ulonglong(*reinterpret_cast<ACE_CDR::ULongLong *>(&m_pHeader->flags)) &&
                     msg.read_ulong(*(reinterpret_cast<ACE_CDR::ULong *>(&m_pHeader->measurement_uid))) &&
                     msg.read_ushort_array(m_pHeader->matrix_size, 3) &&
                     msg.read_float_array(m_pHeader->field_of_view, 3) &&
                     msg.read_ushort(m_pHeader->channels) &&
                     msg.read_float_array(m_pHeader->position, 3) &&
                     msg.read_float_array(m_pHeader->read_dir, 3) &&
                     msg.read_float_array(m_pHeader->phase_dir, 3) &&
                     msg.read_float_array(m_pHeader->slice_dir, 3) &&
                     msg.read_float_array(m_pHeader->patient_table_position, 3) &&
                     msg.read_ushort(m_pHeader->average) &&
                     msg.read_ushort(m_pHeader->slice) &&
                     msg.read_ushort(m_pHeader->contrast) &&
                     msg.read_ushort(m_pHeader->phase) &&
                     msg.read_ushort(m_pHeader->repetition) &&
                     msg.read_ushort(m_pHeader->set) &&
                     msg.read_ulong(*(reinterpret_cast<ACE_CDR::ULong *>(&m_pHeader->acquisition_time_stamp))) &&
                     msg.read_ulong_array(reinterpret_cast<ACE_CDR::ULong *>(m_pHeader->physiology_time_stamp),
                                          ISMRMRD::ISMRMRD_PHYS_STAMPS) &&
                     msg.read_ushort(m_pHeader->image_type) &&
                     msg.read_ushort(m_pHeader->image_index) &&
                     msg.read_ushort(m_pHeader->image_series_index) &&
                     msg.read_long_array(reinterpret_cast<ACE_CDR::Long *>(m_pHeader->user_int), ISMRMRD::ISMRMRD_USER_INTS) &&
                     msg.read_float_array(m_pHeader->user_float, ISMRMRD::ISMRMRD_USER_FLOATS) &&
                     msg.read_ulong(*(reinterpret_cast<ACE_CDR::ULong *>(&m_pHeader->attribute_string_len))) &&
                     msg.read_string(attributes));

      if (retVal)
      {
         m_pAttributes->assign(attributes.c_str(), attributes.length());
         m_pHeader->attribute_string_len = static_cast<uint32_t>(m_pAttributes->length());
      }

      if (m_pHeader->version != ISMRMRD_VERSION_MAJOR)
      {
         WARN_STREAM(ISMRMRD_VERSION_WARNING + " Header deserialization may not work.");
      }

      return retVal;
   }

   bool IsmrmrdImageMessageParser::serialize(ACE_OutputCDR &msg)
   {
      m_pHeader->attribute_string_len = static_cast<uint32_t>(m_pAttributes->length());

      if (m_pHeader->version != ISMRMRD_VERSION_MAJOR)
      {
         WARN_STREAM(ISMRMRD_VERSION_WARNING + " Header serialization may not work.");
      }

      return (msg.write_ushort(m_pHeader->version) &&
              msg.write_ushort(m_pHeader->data_type) &&
              msg.write_ulonglong(m_pHeader->flags) &&
              msg.write_ulong(m_pHeader->measurement_uid) &&
              msg.write_ushort_array(m_pHeader->matrix_size, 3) &&
              msg.write_float_array(m_pHeader->field_of_view, 3) &&
              msg.write_ushort(m_pHeader->channels) &&
              msg.write_float_array(m_pHeader->position, 3) &&
              msg.write_float_array(m_pHeader->read_dir, 3) &&
              msg.write_float_array(m_pHeader->phase_dir, 3) &&
              msg.write_float_array(m_pHeader->slice_dir, 3) &&
              msg.write_float_array(m_pHeader->patient_table_position, 3) &&
              msg.write_ushort(m_pHeader->average) &&
              msg.write_ushort(m_pHeader->slice) &&
              msg.write_ushort(m_pHeader->contrast) &&
              msg.write_ushort(m_pHeader->phase) &&
              msg.write_ushort(m_pHeader->repetition) &&
              msg.write_ushort(m_pHeader->set) &&
              msg.write_ulong(m_pHeader->acquisition_time_stamp) &&
              msg.write_ulong_array(reinterpret_cast<const ACE_CDR::ULong *>(m_pHeader->physiology_time_stamp),
                                    ISMRMRD::ISMRMRD_PHYS_STAMPS) &&
              msg.write_ushort(m_pHeader->image_type) &&
              msg.write_ushort(m_pHeader->image_index) &&
              msg.write_ushort(m_pHeader->image_series_index) &&
              msg.write_long_array(reinterpret_cast<const ACE_CDR::Long *>(m_pHeader->user_int),
                                   ISMRMRD::ISMRMRD_USER_INTS) &&
              msg.write_float_array(m_pHeader->user_float, ISMRMRD::ISMRMRD_USER_FLOATS) &&
              msg.write_ulong(m_pHeader->attribute_string_len) &&
              msg.write_string(static_cast<ACE_CDR::ULong>(m_pAttributes->length()), m_pAttributes->c_str()));
   }

   bool IsmrmrdAcquisitionMessageParser::deserialize(ACE_InputCDR &msg)
   {
      bool retVal = (msg.read_ushort(m_pHeader->version) &&
                     // Some int type madness requires explicit casting
                     msg.read_ulonglong(*reinterpret_cast<ACE_CDR::ULongLong *>(&m_pHeader->flags)) &&
                     msg.read_ulong(*(reinterpret_cast<ACE_CDR::ULong *>(&m_pHeader->measurement_uid))) &&
                     msg.read_ulong(*(reinterpret_cast<ACE_CDR::ULong *>(&m_pHeader->scan_counter))) &&
                     msg.read_ulong(*(reinterpret_cast<ACE_CDR::ULong *>(&m_pHeader->acquisition_time_stamp))) &&
                     msg.read_ulong_array(reinterpret_cast<ACE_CDR::ULong *>(m_pHeader->physiology_time_stamp),
                                          ISMRMRD::ISMRMRD_PHYS_STAMPS) &&
                     msg.read_ushort(m_pHeader->number_of_samples) &&
                     msg.read_ushort(m_pHeader->available_channels) &&
                     msg.read_ushort(m_pHeader->active_channels) &&
                     // Some int type madness requires explicit casting
                     msg.read_ulonglong_array(reinterpret_cast<ACE_CDR::ULongLong *>(m_pHeader->channel_mask),
                                              ISMRMRD::ISMRMRD_CHANNEL_MASKS) &&
                     msg.read_ushort(m_pHeader->discard_pre) &&
                     msg.read_ushort(m_pHeader->discard_post) &&
                     msg.read_ushort(m_pHeader->center_sample) &&
                     msg.read_ushort(m_pHeader->encoding_space_ref) &&
                     msg.read_ushort(m_pHeader->trajectory_dimensions) &&
                     msg.read_float(m_pHeader->sample_time_us) &&
                     msg.read_float_array(m_pHeader->position, 3) &&
                     msg.read_float_array(m_pHeader->read_dir, 3) &&
                     msg.read_float_array(m_pHeader->phase_dir, 3) &&
                     msg.read_float_array(m_pHeader->slice_dir, 3) &&
                     msg.read_float_array(m_pHeader->patient_table_position, 3) &&
                     read_ismrmrd_encoding_counters(msg, m_pHeader->idx) &&
                     msg.read_long_array(reinterpret_cast<ACE_CDR::Long *>(m_pHeader->user_int), ISMRMRD::ISMRMRD_USER_INTS) &&
                     msg.read_float_array(m_pHeader->user_float, ISMRMRD::ISMRMRD_USER_FLOATS));

      if (m_pHeader->version != ISMRMRD_VERSION_MAJOR)
      {
         WARN_STREAM(ISMRMRD_VERSION_WARNING + " Header deserialization may not work.");
      }

      return retVal;
   }

   bool IsmrmrdAcquisitionMessageParser::serialize(ACE_OutputCDR &msg)
   {
      if (m_pHeader->version != ISMRMRD_VERSION_MAJOR)
      {
         WARN_STREAM(ISMRMRD_VERSION_WARNING + " Header serialization may not work.");
      }

      return (msg.write_ushort(m_pHeader->version) &&
              msg.write_ulonglong(m_pHeader->flags) &&
              msg.write_ulong(m_pHeader->measurement_uid) &&
              msg.write_ulong(m_pHeader->scan_counter) &&
              msg.write_ulong(m_pHeader->acquisition_time_stamp) &&
              msg.write_ulong_array(reinterpret_cast<const ACE_CDR::ULong *>(m_pHeader->physiology_time_stamp),
                                    ISMRMRD::ISMRMRD_PHYS_STAMPS) &&
              msg.write_ushort(m_pHeader->number_of_samples) &&
              msg.write_ushort(m_pHeader->available_channels) &&
              msg.write_ushort(m_pHeader->active_channels) &&
              // Some int type madness requires explicit casting
              msg.write_ulonglong_array(reinterpret_cast<const ACE_CDR::ULongLong *>(m_pHeader->channel_mask),
                                        ISMRMRD::ISMRMRD_CHANNEL_MASKS) &&
              msg.write_ushort(m_pHeader->discard_pre) &&
              msg.write_ushort(m_pHeader->discard_post) &&
              msg.write_ushort(m_pHeader->center_sample) &&
              msg.write_ushort(m_pHeader->encoding_space_ref) &&
              msg.write_ushort(m_pHeader->trajectory_dimensions) &&
              msg.write_float(m_pHeader->sample_time_us) &&
              msg.write_float_array(m_pHeader->position, 3) &&
              msg.write_float_array(m_pHeader->read_dir, 3) &&
              msg.write_float_array(m_pHeader->phase_dir, 3) &&
              msg.write_float_array(m_pHeader->slice_dir, 3) &&
              msg.write_float_array(m_pHeader->patient_table_position, 3) &&
              write_ismrmrd_encoding_counters(msg, m_pHeader->idx) &&
              msg.write_long_array(reinterpret_cast<const ACE_CDR::Long *>(m_pHeader->user_int),
                                   ISMRMRD::ISMRMRD_USER_INTS) &&
              msg.write_float_array(m_pHeader->user_float, ISMRMRD::ISMRMRD_USER_FLOATS));
   }

   ACE_CDR::Boolean IsmrmrdAcquisitionMessageParser::write_ismrmrd_encoding_counters(ACE_OutputCDR &msg, const ISMRMRD::ISMRMRD_EncodingCounters &counter)
   {
      return (msg.write_ushort(counter.kspace_encode_step_1) &&
              msg.write_ushort(counter.kspace_encode_step_2) &&
              msg.write_ushort(counter.average) &&
              msg.write_ushort(counter.slice) &&
              msg.write_ushort(counter.contrast) &&
              msg.write_ushort(counter.phase) &&
              msg.write_ushort(counter.repetition) &&
              msg.write_ushort(counter.set) &&
              msg.write_ushort(counter.segment) &&
              msg.write_ushort_array(reinterpret_cast<const ACE_CDR::UShort *>(counter.user),
                                     ISMRMRD::ISMRMRD_USER_INTS));
   }

   ACE_CDR::Boolean IsmrmrdAcquisitionMessageParser::read_ismrmrd_encoding_counters(ACE_InputCDR &msg, ISMRMRD::ISMRMRD_EncodingCounters &counter)
   {
      return (msg.read_ushort(counter.kspace_encode_step_1) &&
              msg.read_ushort(counter.kspace_encode_step_2) &&
              msg.read_ushort(counter.average) &&
              msg.read_ushort(counter.slice) &&
              msg.read_ushort(counter.contrast) &&
              msg.read_ushort(counter.phase) &&
              msg.read_ushort(counter.repetition) &&
              msg.read_ushort(counter.set) &&
              msg.read_ushort(counter.segment) &&
              msg.read_ushort_array(counter.user, ISMRMRD::ISMRMRD_USER_INTS));
   }

#pragma clang diagnostic pop

   ACE_Message_Block *IsmrmrdAcquisitionMessageParser::serialize(ISMRMRD::AcquisitionHeader const &header, ACE_Message_Block *data, ACE_Message_Block *traj)
   {
      m_pHeader = const_cast<ISMRMRD::AcquisitionHeader *>(&header);
      if (nullptr != data)
         get_last(data)->cont(traj);
      return MessageParser::serialize(data);
   }

   ACE_Message_Block *IsmrmrdAcquisitionMessageParser::serialize(ISMRMRD::Acquisition const &acquisition)
   {
      // Full copy of data and trajectory are made because acquisition may drop out of scope before message is used
      ACE_Message_Block *data = nullptr;
      ACE_NEW_NORETURN(data, ACE_Message_Block(acquisition.getDataSize() + acquisition.getTrajSize()));
      if (nullptr == data)
      {
         return nullptr;
      }

      memcpy(data->wr_ptr(), acquisition.getDataPtr(), acquisition.getDataSize());
      data->wr_ptr(acquisition.getDataSize());

      memcpy(data->wr_ptr(), acquisition.getTrajPtr(), acquisition.getTrajSize());
      data->wr_ptr(acquisition.getTrajSize());

      return serialize(acquisition.getHead(), data);
   }

   bool IsmrmrdAcquisitionMessageParser::deserialize(ACE_Message_Block *mb, ISMRMRD::AcquisitionHeader &header)
   {
      m_pHeader = &header;
      return MessageParser::deserialize(mb);
   }

   bool IsmrmrdAcquisitionMessageParser::deserialize(ACE_Message_Block *mb, ISMRMRD::Acquisition &acquisition)
   {
      ISMRMRD::AcquisitionHeader &header = const_cast<ISMRMRD::AcquisitionHeader &>(acquisition.getHead());

      if (!deserialize(mb, header))
         return false;

      acquisition.setHead(header);

      // Check for sufficient data exists
      if (mb->total_length() < (acquisition.getDataSize() + acquisition.getTrajSize()))
         return false;

      if (!copy(reinterpret_cast<char *>(acquisition.getDataPtr()), mb, acquisition.getDataSize()) ||
          !copy(reinterpret_cast<char *>(acquisition.getTrajPtr()), mb, acquisition.getTrajSize()))
         return false;

      if (swap())
      {
         network::byteswap(acquisition.getDataPtr(), acquisition.getNumberOfDataElements());

         if (acquisition.getTrajSize() > 0)
            network::byteswap(acquisition.getTrajPtr(), acquisition.getNumberOfTrajElements());
      }

      return true;
   }

}
