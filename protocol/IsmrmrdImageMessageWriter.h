//
// Created by Martyn Klassen on 2019-02-11.
//

#ifndef ACE_ISMRMRD_TOOLKIT_ISMRMRDIMAGEMESSAGEWRITER_H
#define ACE_ISMRMRD_TOOLKIT_ISMRMRDIMAGEMESSAGEWRITER_H

#include "MessageWriter.h"
#include "MessageParser.h"
#include "GadgetronMessages.h"

#include <ismrmrd/ismrmrd.h>


namespace network
{
   class IsmrmrdImageMessageWriter : virtual public MessageWriter
   {
   public:
      explicit IsmrmrdImageMessageWriter(StreamInterface const &stream)
            : MessageWriter(stream)
      {}

      explicit IsmrmrdImageMessageWriter(ACE_SOCK_Stream *stream)
            : MessageWriter(stream)
      {}

      virtual ~IsmrmrdImageMessageWriter()
      {}

      virtual int write(ISMRMRD::ImageHeader const &header, std::string const &attributes, ACE_Message_Block *data)
      {
         size_t requiredSize = header.matrix_size[0] * header.matrix_size[1] * header.matrix_size[2] *
                               header.channels * ISMRMRD::ismrmrd_sizeof_data_type(header.data_type);

         if (data->total_length() != requiredSize)
            return -1;

         IsmrmrdImageMessageParser parser;
         ACE_Message_Block *mb = parser.serialize(header, attributes, data);
         if (mb == nullptr)
            return -1;
         return MessageWriter::write(mb);
      }

      template<typename T>
      int write(ISMRMRD::ImageHeader const &header, std::string const &attributes, T *data, size_t elements)
      {
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_RETURN(mb, ACE_Message_Block(reinterpret_cast<char const *>(data), elements * sizeof(*data)), -1);
         mb->wr_ptr(elements * sizeof(*data));

         return write(header, attributes, mb);
      }

   protected:
      virtual ACE_UINT16 message_id() const
      { return GADGET_MESSAGE_ISMRMRD_IMAGE; }
   };
}

#endif //ACE_ISMRMRD_TOOLKIT_ISMRMRDIMAGEMESSAGEWRITER_H
