//
// Created by Martyn Klassen on 2019-02-11.
//

#ifndef ACE_ISMRMRD_TOOLKIT_ISMRMRDACQUISITIONMESSAGEWRITER_H
#define ACE_ISMRMRD_TOOLKIT_ISMRMRDACQUISITIONMESSAGEWRITER_H

#include "MessageWriter.h"
#include "MessageParser.h"
#include "GadgetronMessages.h"

#include <ismrmrd/ismrmrd.h>


namespace network
{
   class IsmrmrdAcquisitionMessageWriter : virtual public MessageWriter
   {
   public:
      explicit IsmrmrdAcquisitionMessageWriter(StreamInterface const &stream)
            : MessageWriter(stream)
      {}

      explicit IsmrmrdAcquisitionMessageWriter(ACE_SOCK_Stream *stream)
            : MessageWriter(stream)
      {}

      virtual ~IsmrmrdAcquisitionMessageWriter()
      {}

      virtual ACE_UINT16 message_id() const
      { return GADGET_MESSAGE_ISMRMRD_ACQUISITION; }

      virtual int write(ISMRMRD::Acquisition const &acquisition)
      {
         ACE_Message_Block *data = nullptr;

         ACE_NEW_RETURN(data, ACE_Message_Block(reinterpret_cast<char const *>(acquisition.getDataPtr()), acquisition.getDataSize()), -1);
         data->wr_ptr(acquisition.getDataSize());

         ISMRMRD::AcquisitionHeader const &header(acquisition.getHead());

         if (header.trajectory_dimensions > 0)
         {
            ACE_Message_Block *traj = nullptr;
            ACE_NEW_NORETURN(traj, ACE_Message_Block(reinterpret_cast<char const *>(acquisition.getTrajPtr()), acquisition.getTrajSize()));

            if (nullptr == traj)
            {
               data->release();
               return -1;
            }
            traj->wr_ptr(acquisition.getTrajSize());
            data->cont(traj);
         }

         return write(acquisition.getHead(), data);
      }

      virtual int write(ISMRMRD::AcquisitionHeader const &header, ACE_Message_Block *data)
      {
         size_t requiredSize = header.number_of_samples * header.active_channels * sizeof(complex_float_t) +
                               header.trajectory_dimensions * header.number_of_samples * sizeof(float);

         if (requiredSize != data->total_length())
            return -1;

         IsmrmrdAcquisitionMessageParser parser;
         ACE_Message_Block *mb = parser.serialize(header, data);
         if (mb == nullptr)
            return -1;
         return MessageWriter::write(mb);
      }

      virtual int write(ISMRMRD::AcquisitionHeader const &header, ACE_Message_Block *data, ACE_Message_Block *traj)
      {
         if (nullptr != data)
         {
            // Add the trajectory to the end of the data
            MessageParser::get_last(data)->cont(traj);
         }
         else
         {
            data = traj;
         }

         return write(header, data);
      }
   };
}

#endif //ACE_ISMRMRD_TOOLKIT_ISMRMRDACQUISITIONMESSAGEWRITER_H
