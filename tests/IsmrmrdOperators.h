//
// Created by Martyn Klassen on 2019-02-28.
//

#ifndef GADGETRON_ISMRMRDOPERATORS_H
#define GADGETRON_ISMRMRDOPERATORS_H

#include "ismrmrd/ismrmrd.h"
#include "ismrmrd/version.h"
#include "ismrmrd/meta.h"


namespace ISMRMRD
{
   bool operator==(ISMRMRD::MetaValue const &lhs, ISMRMRD::MetaValue const &rhs)
   {
      return (lhs.as_double() == rhs.as_double() && lhs.as_long() == rhs.as_long() && lhs.as_str() == rhs.as_str());
   }

   bool operator==(ISMRMRD::MetaContainer const &lhs, ISMRMRD::MetaContainer const &rhs)
   {
      auto liter = lhs.begin();

      try
      {
         while (liter != lhs.end())
         {
            for (size_t i = 0; i < liter->second.size(); i++)
               if (!(liter->second[i] == rhs.value(liter->first.c_str(), i))) return false;
            liter++;
         }
      }
      catch (std::runtime_error &)
      {
         return false;
      }

      return true;
   }
}

#endif //GADGETRON_ISMRMRDOPERATORS_H
