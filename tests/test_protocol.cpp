//
// Created by Igor Solovey on 2019-02-01.
//

#include "gmock/gmock.h"
#include "IsmrmrdOperators.h"

#include "IsmrmrdAcquisitionMessageWriter.h"
#include "IsmrmrdAcquisitionMessageReader.h"
#include "IsmrmrdImageMessageWriter.h"
#include "IsmrmrdImageMessageReader.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"


namespace
{
   using ::testing::HasSubstr;
   class MockStream : public StreamInterface
   {
   public:
      MockStream()
            : StreamInterface(nullptr)
      {}

      ~MockStream() override
      {
         clear();
      }

      ssize_t send_n(ACE_Message_Block const *mb) const override
      {
         if (m_pMsg)
            network::MessageParser::get_last(m_pMsg)->cont(mb->duplicate());
         else
         {
            m_pMsg = mb->duplicate();
            m_pMsg->rd_ptr(sizeof(ACE_UINT16));
         }

         return mb->total_length();
      }

      ssize_t recv_n(void *buf, size_t len) const override
      {
         if (!network::MessageParser::copy(reinterpret_cast<char *>(buf), m_pMsg, len))
            return -1;

         return len;
      }

      void clear()
      {
         if (m_pMsg)
         {
            m_pMsg->release();
            m_pMsg = nullptr;
         }
      }

   private:
      static ACE_Message_Block *m_pMsg;
   };

   ACE_Message_Block *MockStream::m_pMsg = nullptr;

   class ProtocolTest : public ::testing::Test
   {
   protected:
      ProtocolTest()
      {
         m_AcquisitionHeader.version = ISMRMRD_VERSION_MAJOR;
         m_AcquisitionHeader.flags = 10;
         m_AcquisitionHeader.measurement_uid = 3332;
         m_AcquisitionHeader.scan_counter = 4;
         m_AcquisitionHeader.acquisition_time_stamp = 70;
         for (uint32_t i = 0; i < ISMRMRD::ISMRMRD_PHYS_STAMPS; i++)
            m_AcquisitionHeader.physiology_time_stamp[i] = i;
         m_AcquisitionHeader.number_of_samples = 10;
         m_AcquisitionHeader.available_channels = 3;
         m_AcquisitionHeader.active_channels = 2;
         for (size_t i = 0; i < ISMRMRD::ISMRMRD_CHANNEL_MASKS; i++)
            m_AcquisitionHeader.channel_mask[i] = i;
         m_AcquisitionHeader.discard_pre = 5;
         m_AcquisitionHeader.discard_pre = 9;
         m_AcquisitionHeader.center_sample = 17;
         m_AcquisitionHeader.encoding_space_ref = 21;
         m_AcquisitionHeader.trajectory_dimensions = 1;

         m_Acquisition.setHead(m_AcquisitionHeader);

         for (size_t i = 0; i < m_Acquisition.getNumberOfDataElements(); i++)
            m_Acquisition.getDataPtr()[i] = i;

         for (size_t i = 0; i < m_Acquisition.getNumberOfTrajElements(); i++)
            m_Acquisition.getTrajPtr()[i] = i;

         m_attributes = "abc-testing";

         m_ImageHeader.version = ISMRMRD_VERSION_MAJOR;
         m_ImageHeader.data_type = ISMRMRD::ISMRMRD_CXDOUBLE;
         m_ImageHeader.flags = m_AcquisitionHeader.flags;
         m_ImageHeader.measurement_uid = m_AcquisitionHeader.measurement_uid;
         m_ImageHeader.matrix_size[0] = 1;
         m_ImageHeader.matrix_size[1] = 1;
         m_ImageHeader.matrix_size[2] = 1;
         m_ImageHeader.field_of_view[0] = 1.0f;
         m_ImageHeader.field_of_view[1] = 1.0f;
         m_ImageHeader.field_of_view[2] = 1.0f;
         m_ImageHeader.channels = m_AcquisitionHeader.active_channels;
         m_ImageHeader.position[0] = 0.0f;
         m_ImageHeader.position[1] = 0.0f;
         m_ImageHeader.position[2] = 0.0f;
         m_ImageHeader.read_dir[0] = 1.0f;
         m_ImageHeader.read_dir[1] = 0.0f;
         m_ImageHeader.read_dir[2] = 0.0f;
         m_ImageHeader.phase_dir[0] = 0.0f;
         m_ImageHeader.phase_dir[1] = 1.0f;
         m_ImageHeader.phase_dir[2] = 0.0f;
         m_ImageHeader.slice_dir[0] = 0.0f;
         m_ImageHeader.slice_dir[1] = 0.0f;
         m_ImageHeader.slice_dir[2] = 1.0f;
         m_ImageHeader.patient_table_position[0] = 0.0f;
         m_ImageHeader.patient_table_position[1] = 0.0f;
         m_ImageHeader.patient_table_position[2] = 0.0f;
         m_ImageHeader.average = 0;
         m_ImageHeader.slice = 1;
         m_ImageHeader.contrast = 2;
         m_ImageHeader.phase = 3;
         m_ImageHeader.repetition = 4;
         m_ImageHeader.set = 5;

         m_Image.setHead(m_ImageHeader);
         std::stringstream sstream;
         ISMRMRD::serialize(m_Meta, sstream);
         m_attributes = sstream.str();
         m_Image.setAttributeString(m_attributes);
      }

      ~ProtocolTest() = default;

      ACE_Message_Block *acquisitionData()
      {
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_RETURN(mb, ACE_Message_Block(reinterpret_cast<char const *>(m_Acquisition.getDataPtr()), m_Acquisition.getDataSize()), nullptr);

         mb->wr_ptr(m_Acquisition.getDataSize());
         return mb;
      }

      ACE_Message_Block *trajectoryData()
      {
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_RETURN(mb, ACE_Message_Block(reinterpret_cast<char const *>(m_Acquisition.getTrajPtr()), m_Acquisition.getTrajSize()), nullptr);

         mb->wr_ptr(m_Acquisition.getTrajSize());
         return mb;
      }

      ACE_Message_Block *imageData()
      {
         ACE_Message_Block *mb = nullptr;
         ACE_NEW_RETURN(mb, ACE_Message_Block(reinterpret_cast<char const *>(m_Image.getDataPtr()), m_Image.getDataSize()), nullptr);

         mb->wr_ptr(m_Image.getDataSize());
         return mb;
      }

      template<typename T>
      void writeReadImageTemplate()
      {
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnusedValue"
         ISMRMRD::ImageHeader input_header = m_ImageHeader;
         if (typeid(T) == typeid(uint16_t))
            input_header.data_type = ISMRMRD::ISMRMRD_USHORT;
         else if (typeid(T) == typeid(int16_t))
            input_header.data_type = ISMRMRD::ISMRMRD_SHORT;
         else if (typeid(T) == typeid(uint32_t))
            input_header.data_type = ISMRMRD::ISMRMRD_UINT;
         else if (typeid(T) == typeid(int32_t))
            input_header.data_type = ISMRMRD::ISMRMRD_INT;
         else if (typeid(T) == typeid(float))
            input_header.data_type = ISMRMRD::ISMRMRD_FLOAT;
         else if (typeid(T) == typeid(double))
            input_header.data_type = ISMRMRD::ISMRMRD_DOUBLE;
         else if (typeid(T) == typeid(complex_float_t))
            input_header.data_type = ISMRMRD::ISMRMRD_CXFLOAT;
         else if (typeid(T) == typeid(complex_double_t))
            input_header.data_type = ISMRMRD::ISMRMRD_CXDOUBLE;
#pragma clang diagnostic pop

#if 0
         ISMRMRD_USHORT   = 1, /**< corresponds to uint16_t */
         ISMRMRD_SHORT    = 2, /**< corresponds to int16_t */
         ISMRMRD_UINT     = 3, /**< corresponds to uint32_t */
         ISMRMRD_INT      = 4, /**< corresponds to int32_t */
         ISMRMRD_FLOAT    = 5, /**< corresponds to float */
         ISMRMRD_DOUBLE   = 6, /**< corresponds to double */
         ISMRMRD_CXFLOAT  = 7, /**< corresponds to complex float */
         ISMRMRD_CXDOUBLE = 8  /**< corresponds to complex double */
#endif

         MockStream stream;

         ISMRMRD::Image<T> input_image;

         network::IsmrmrdImageMessageWriter writer(stream);

         ASSERT_NE(writer.write(m_ImageHeader, m_attributes, m_Image.getDataPtr(), m_Image.getNumberOfDataElements()), -1);
         network::IsmrmrdImageMessageReader reader(stream);

         ISMRMRD::ImageHeader header;
         ISMRMRD::MetaContainer meta;
         ASSERT_NE(reader.read(header, meta), -1);

         std::stringstream sstream;
         ISMRMRD::serialize(meta, sstream);

         ASSERT_EQ(sstream.str(), m_attributes);

         ASSERT_EQ(header, m_ImageHeader);

         ISMRMRD::Image<complex_double_t> image;

         image.setHead(header);
         reader.read(image.getDataPtr(), image.getNumberOfDataElements());

         ASSERT_EQ(image.getHead(), m_Image.getHead());
         ASSERT_EQ(image, m_Image);
      }

      ISMRMRD::AcquisitionHeader m_AcquisitionHeader;
      ISMRMRD::Acquisition m_Acquisition;

      ISMRMRD::ImageHeader m_ImageHeader;
      ISMRMRD::MetaContainer m_Meta;
      std::string m_attributes;

      ISMRMRD::Image<complex_double_t> m_Image;

   };

   TEST_F(ProtocolTest, parseAcquisitionHeader) // NOLINT
   {
      network::IsmrmrdAcquisitionMessageParser parser;
      ACE_Message_Block *mb = parser.serialize(m_AcquisitionHeader);

      ASSERT_NE(mb, nullptr) << "failed to serialize acquisition header";

      ISMRMRD::AcquisitionHeader retrieved_header;
      ASSERT_TRUE(parser.deserialize(mb, retrieved_header)) << "failed to deserialize acquisition header";
      mb->release();
      ASSERT_EQ(m_AcquisitionHeader, retrieved_header);
   }

   TEST_F(ProtocolTest, parseAcquisitionHeaderVersionCheck) // NOLINT
   {
      network::IsmrmrdAcquisitionMessageParser parser;
      ISMRMRD::AcquisitionHeader header(m_AcquisitionHeader);
      header.version = 2;
      std::stringstream buffer;
      std::streambuf *sbuf = std::cerr.rdbuf();
      std::cerr.rdbuf(buffer.rdbuf());

      ACE_Message_Block *mb = parser.serialize(header);

      ASSERT_THAT(buffer.str().c_str(), HasSubstr(ISMRMRD_VERSION_WARNING));
      buffer.clear();

      ISMRMRD::AcquisitionHeader retrieved_header;
      parser.deserialize(mb, retrieved_header);

      ASSERT_THAT(buffer.str().c_str(), HasSubstr(ISMRMRD_VERSION_WARNING));

      mb->release();
      std::cerr.rdbuf(sbuf);
   }

   TEST_F(ProtocolTest, parseImageHeader) // NOLINT
   {
      network::IsmrmrdImageMessageParser parser;
      ACE_Message_Block *mb = parser.serialize(m_ImageHeader, m_attributes);

      ASSERT_NE(mb, nullptr) << "failed to serialize image header";

      ISMRMRD::ImageHeader retrieved_header;
      std::string retrieved_attributes;
      ASSERT_TRUE(parser.deserialize(mb, retrieved_header, retrieved_attributes)) << "failed to deserialize image header";
      mb->release();
      ASSERT_EQ(m_ImageHeader, retrieved_header);
      ASSERT_EQ(m_attributes, retrieved_attributes);
   }

   TEST_F(ProtocolTest, parseImageHeaderVersionCheck) // NOLINT
   {
      network::IsmrmrdImageMessageParser parser;
      ISMRMRD::ImageHeader header(m_ImageHeader);
      header.version = 2;
      std::stringstream buffer;
      std::streambuf *sbuf = std::cerr.rdbuf();
      std::cerr.rdbuf(buffer.rdbuf());

      ACE_Message_Block *mb = parser.serialize(header, m_attributes);

      ASSERT_THAT(buffer.str().c_str(), HasSubstr(ISMRMRD_VERSION_WARNING));
      buffer.clear();

      ISMRMRD::ImageHeader retrieved_header;
      std::string retrieved_attributes;
      parser.deserialize(mb, retrieved_header, retrieved_attributes);

      ASSERT_THAT(buffer.str().c_str(), HasSubstr(ISMRMRD_VERSION_WARNING));

      mb->release();
      std::cerr.rdbuf(sbuf);
   }

   TEST_F(ProtocolTest, parseAcquisition) // NOLINT
   {
      network::IsmrmrdAcquisitionMessageParser parser;
      ACE_Message_Block *mb = parser.serialize(m_Acquisition);

      ASSERT_NE(mb, nullptr) << "failed to serialize acquisition";

      ISMRMRD::Acquisition retrieved_acquisition;

      ASSERT_TRUE(parser.deserialize(mb, retrieved_acquisition)) << "failed to deserialize acquisition";
      mb->release();
      ASSERT_EQ(m_Acquisition, retrieved_acquisition);
   }

   TEST_F(ProtocolTest, parseAcquisitionSplit) // NOLINT
   {
      ACE_Message_Block *data = acquisitionData();
      ACE_Message_Block *traj = trajectoryData();

      network::IsmrmrdAcquisitionMessageParser parser;
      ACE_Message_Block *mb = parser.serialize(m_Acquisition.getHead(), data, traj);
      ASSERT_NE(mb, nullptr) << "failed to serialize split acquisition";

      ISMRMRD::Acquisition retrieved_acquisition;
      ASSERT_TRUE(parser.deserialize(mb, retrieved_acquisition)) << "failed to deserialize split acquisition";
      mb->release();
      ASSERT_EQ(m_Acquisition, retrieved_acquisition);
   }


   TEST_F(ProtocolTest, writeReadAcquisition) // NOLINT
   {
      MockStream stream;

      network::IsmrmrdAcquisitionMessageWriter writer(stream);
      ASSERT_NE(writer.write(m_Acquisition), -1);

      network::IsmrmrdAcquisitionMessageReader reader(stream);
      ISMRMRD::Acquisition acquisition;

      ASSERT_NE(reader.read(acquisition), -1);
      ASSERT_EQ(acquisition, m_Acquisition);

      stream.clear();

      ACE_Message_Block *data = acquisitionData();
      ACE_Message_Block *traj = trajectoryData();
      data->cont(traj);
      ASSERT_NE(writer.write(m_Acquisition.getHead(), data), -1);

      ISMRMRD::AcquisitionHeader header;
      ASSERT_NE(reader.read(header), -1);
      ASSERT_EQ(header, m_Acquisition.getHead());

      acquisition.setHead(header);
      ASSERT_NE(reader.read(acquisition.getDataPtr(), acquisition.getNumberOfDataElements()), -1);
      ASSERT_NE(reader.read(acquisition.getTrajPtr(), acquisition.getNumberOfTrajElements()), -1);
      ASSERT_EQ(acquisition, m_Acquisition);

      stream.clear();

      data = acquisitionData();
      traj = trajectoryData();
      ASSERT_NE(writer.write(m_Acquisition.getHead(), data, traj), -1);

      ISMRMRD::NDArray<complex_float_t> read_data;
      ISMRMRD::NDArray<float> read_traj;
      ASSERT_NE(reader.read(header, read_data, read_traj), -1);

      acquisition.setHead(header);
      ASSERT_EQ(acquisition.getDataSize(), read_data.getDataSize());
      ASSERT_EQ(acquisition.getTrajSize(), read_traj.getDataSize());

      memcpy(acquisition.getDataPtr(), read_data.getDataPtr(), read_data.getDataSize());
      memcpy(acquisition.getTrajPtr(), read_traj.getDataPtr(), read_traj.getDataSize());

      ASSERT_EQ(acquisition, m_Acquisition);

   }

   TEST_F(ProtocolTest, writeReadImage) // NOLINT
   {
      MockStream stream;
      network::IsmrmrdImageMessageWriter writer(stream);
      ACE_Message_Block *data = imageData();

      ASSERT_NE(writer.write(m_ImageHeader, m_attributes, data), -1);

      network::IsmrmrdImageMessageReader reader(stream);

      ISMRMRD::ImageHeader header;
      ISMRMRD::MetaContainer meta;
      ASSERT_NE(reader.read(header, meta), -1);

      std::stringstream sstream;
      ISMRMRD::serialize(meta, sstream);

      ASSERT_EQ(sstream.str(), m_attributes);

      ASSERT_EQ(header, m_ImageHeader);

      ISMRMRD::Image<complex_double_t> image;

      image.setHead(header);
      reader.read(image.getDataPtr(), image.getNumberOfDataElements());

      ASSERT_EQ(image.getHead(), m_Image.getHead());
      ASSERT_EQ(image, m_Image);

      stream.clear();

      ASSERT_NE(writer.write(m_ImageHeader, m_attributes, m_Image.getDataPtr(), m_Image.getNumberOfDataElements()), -1);


      ASSERT_NE(reader.read(header, meta), -1);

      sstream.str("");
      ISMRMRD::serialize(meta, sstream);

      ASSERT_EQ(sstream.str(), m_attributes);

      ASSERT_EQ(header, m_ImageHeader);

      image.setHead(header);
      reader.read(image.getDataPtr(), image.getNumberOfDataElements());

      ASSERT_EQ(image.getHead(), m_Image.getHead());
      ASSERT_EQ(image, m_Image);
   }

   TEST_F(ProtocolTest, writeReadImageTests) // NOLINT
   {
      writeReadImageTemplate<uint16_t>();
      writeReadImageTemplate<int16_t>();
      writeReadImageTemplate<uint32_t>();
      writeReadImageTemplate<int32_t>();
      writeReadImageTemplate<float>();
      writeReadImageTemplate<double>();
      writeReadImageTemplate<complex_float_t>();
      writeReadImageTemplate<complex_double_t>();
   }

}

#pragma clang diagnostic pop
