//
// Created by Igor Solovey on 2019-02-27.
//

#include <MessageParser.h>
#include "gmock/gmock.h"
#include <ace/Acceptor.h>
#include <ace/SOCK_Acceptor.h>

#include "TransportClient.h"
#include "TransportTestController.h"
#include "GadgetronMessages.h"


#define TEST_ACCEPTOR_PORT "3346"

namespace
{
   using namespace network;

   class SignalHandler : public ACE_Event_Handler
   {
   public:
      SignalHandler() = default;
   };

   template<typename Controller>
   class TransportClientTest : public ::testing::Test
   {
   public:
      TransportClientTest()
            : m_client()
      {}

      TransportClientTest(ACE_Reactor *reactor)
            : m_client(nullptr, nullptr, reactor)
      {}

      void SetUp()
      {
         ACE_INET_Addr port_to_listen(TEST_ACCEPTOR_PORT, ACE_LOCALHOST);
         if (m_acceptor.open(port_to_listen) < 0)
         {
            m_acceptor.close();
            ERROR("Cannot use port " + std::string(TEST_ACCEPTOR_PORT));
         }

         std::string iniFile;
         std::string host(ACE_LOCALHOST);
         std::string port(TEST_ACCEPTOR_PORT);
         if (!m_client.connect(iniFile, host, port, &m_processor))
         {
            m_acceptor.close();
            ERROR("Unable to connect to acceptor");
         }
      }

      void run_event_loop_and_cleanup()
      {
         SignalHandler handler;

         ACE_Reactor::instance()->register_handler(SIGPIPE, &handler);

         // Process the event loop until the last thread of the client closes
         ACE_Time_Value time_value(1);
         do
         {
            ACE_Reactor::instance()->run_reactor_event_loop(time_value);
         } while (m_client.thr_count() >= 1);

         // All the client threads are done, so shutdown the acceptor and reactor
         m_acceptor.close();
         m_client.wait();

         ACE_Reactor::instance()->remove_handler(SIGPIPE, nullptr);
      }

   protected:
      ACE_Acceptor<Controller, ACE_SOCK_Acceptor> m_acceptor;
      network::TransportClient m_client;
      network::TransportProcessor m_processor;
   };

   template<typename Controller>
   class TransportClientTestNull : public TransportClientTest<Controller>
   {
   public:
      TransportClientTestNull()
            : TransportClientTest<Controller>(nullptr)
      {}
   };


   class RecvAndSendCloseController : public TransportTestController
   {
   public:
      int handle_input(ACE_HANDLE fd = ACE_INVALID_HANDLE) override
      {
         ACE_UINT16 msg_type;
         if (this->get_message_type(msg_type) == -1)
            return -1;

         if (msg_type != GADGET_MESSAGE_CLOSE)
         {
            ERROR_STREAM("Did not receive GADGET_MESSAGE_CLOSE as expected\n");
            return -1;
         }

         this->send_message(GADGET_MESSAGE_CLOSE);
         return -1;
      }

      int handle_close(ACE_HANDLE handle, ACE_Reactor_Mask mask) override
      {
         DEBUG("Transport Test Controller connection connection closed\n");
         return 0;
      }
   };

   class RecvAndNoOpController : public TransportTestController
   {
   public:
      int handle_input(ACE_HANDLE fd = ACE_INVALID_HANDLE) override
      {
         ACE_UINT16 msg_type;
         if (this->get_message_type(msg_type) == -1)
            return -1;

         if (msg_type != GADGET_MESSAGE_CLOSE)
         {
            ERROR_STREAM("Did not receive GADGET_MESSAGE_CLOSE as expected\n");
            return -1;
         }
         return -1;
      }
   };

   class NoOpController : public TransportTestController
   {
   public:
      int handle_input(ACE_HANDLE fd = ACE_INVALID_HANDLE) override
      {
         return -1;
      }
   };

   using MyTypes = ::testing::Types<
         RecvAndSendCloseController,
         RecvAndNoOpController,
         NoOpController
   >;
   TYPED_TEST_SUITE(TransportClientTest, MyTypes);

   TYPED_TEST(TransportClientTest, clientSendAndClose)
   {
      // send a message to the server
      if (this->m_client.send(GADGET_MESSAGE_CLOSE) == -1) ERROR("Cannot send message");
      this->run_event_loop_and_cleanup();
   }

   TYPED_TEST_SUITE(TransportClientTestNull, MyTypes);

   TYPED_TEST(TransportClientTestNull, clientSendAndClose)
   {
      // send a message to the server
      if (this->m_client.send(GADGET_MESSAGE_CLOSE) == -1) ERROR("Cannot send message");
      this->run_event_loop_and_cleanup();
   }
}
