#ifndef ACE_ISMRMRD_TOOLKIT_TRANSPORTTESTCONTROLLER_H
#define ACE_ISMRMRD_TOOLKIT_TRANSPORTTESTCONTROLLER_H

#include "ace/Svc_Handler.h"
#include "ace/Stream.h"
#include "ace/Reactor_Notification_Strategy.h"
#include "MessageWriter.h"
#include "GadgetronMessages.h"

#define DEBUG(x)
//DEBUG_STREAM(x)
#define ERROR(x) throw std::runtime_error(x)

using namespace network;

class TransportTestController
      : public ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_NULL_SYNCH>
{
public:
   virtual ~TransportTestController(){}

   // default open registers read handler

   int get_message_type(ACE_UINT16 &msg_type){
      size_t recv_cnt;
      if ((recv_cnt = peer().recv_n(&msg_type, sizeof(ACE_UINT16))) <= 0)
      {
         ERROR("Unable to read message type\n");
      }
      msg_type = static_cast<ACE_UINT16>(ACE_NTOHS(msg_type));
      return 0;
   }

   int send_message(ACE_UINT16 msgType, ACE_Message_Block *data = nullptr){
      ACE_Message_Block *send_mb = MessageWriter::wrap(msgType, data);

      if ( this->peer().send_n(send_mb) != send_mb->total_length() ){
         ERROR_STREAM("Unable to send message");
         send_mb->release();
         return -1;
      }
      return 0;
   }

   virtual int handle_close()
   {
      return handle_close(ACE_INVALID_HANDLE, ACE_Event_Handler::ALL_EVENTS_MASK);
   }

   virtual int handle_close(ACE_HANDLE handle)
   {
      return handle_close(handle, ACE_Event_Handler::ALL_EVENTS_MASK);
   }

   int handle_close(ACE_HANDLE handle, ACE_Reactor_Mask mask) override
   {
      DEBUG("Transport Test Controller connection connection closed\n");

      this->shutdown();
      return 0;
   }

};


#endif //ACE_ISMRMRD_TOOLKIT_TRANSPORTTESTCONTROLLER_H
